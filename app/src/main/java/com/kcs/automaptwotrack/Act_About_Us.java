package com.kcs.automaptwotrack;

import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class Act_About_Us extends AppCompatActivity {

    ImageView img_back;
    TextView txt_version, txt_terms_condition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__about__us);

        img_back = findViewById(R.id.img_back);
        txt_version = findViewById(R.id.txt_version);
        txt_terms_condition = findViewById(R.id.txt_terms_condition);


        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        txt_version.setText(Html.fromHtml("<b>Version  </b> "+ BuildConfig.VERSION_NAME));
        txt_terms_condition.setText(Html.fromHtml(getString(R.string.term_condition)));
    }

}
