package com.kcs.automaptwotrack;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.jackandphantom.circularimageview.RoundedImage;
import com.kcs.automaptwotrack.Constant.Constant;
import com.kcs.automaptwotrack.Model.Admin_Msg_Model;
import com.kcs.automaptwotrack.utils.AppUtils;
import com.potyvideo.library.AndExoPlayerView;
import com.potyvideo.library.globalEnums.EnumAspectRatio;
import com.potyvideo.library.globalEnums.EnumResizeMode;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.leolin.shortcutbadger.ShortcutBadger;

public class Act_Admin_Message extends AppCompatActivity implements View.OnClickListener {

    ImageView back, img_refresh;
    TextView txt_title;
    RecyclerView rcv_view;
    View lin_error;
    Button btnRetry;
    TextView txtMsg;
    List<Admin_Msg_Model> admin_msg_models = new ArrayList<>();
    AdapterAdminMsg adapterAdminMsg;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    String tow_track_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__admin__message);

        back = findViewById(R.id.back);
        img_refresh = findViewById(R.id.img_refresh);
        txt_title = findViewById(R.id.txt_title);
        lin_error = findViewById(R.id.lin_error);
        rcv_view = findViewById(R.id.rcy_view);
        txtMsg = findViewById(R.id.txtError);
        btnRetry = findViewById(R.id.btnRetry);
        preferences = getSharedPreferences(Act_Login.LOGIN_PREF, MODE_PRIVATE);

        tow_track_id = preferences.getString(getString(R.string.pref_towtrack_id), "");
        back.setOnClickListener(this);
        btnRetry.setOnClickListener(this);
        img_refresh.setOnClickListener(this);
        AdminMsg();
        InitializeBroadcast();

        /*admin_msg_models = new ArrayList<>();
        admin_msg_models.add(new Admin_Msg_Model("1","sds","s","2020-06-16 12:45:36"));
        admin_msg_models.add(new Admin_Msg_Model("2","sds fdk ffs djs asd skd ksk kdk sad eww iew fdsf","s","2020-06-16 01:45:36"));
        admin_msg_models.add(new Admin_Msg_Model("3","fie lsa djs ad sa dj ajd sas js jas asd ksa d","","2020-06-16 09:45:36"));
        admin_msg_models.add(new Admin_Msg_Model("4","des ksd kf sdk dsf d","s","2020-03-16 03:11:36"));

        adapterAdminMsg = new AdapterAdminMsg(admin_msg_models, getApplicationContext());
        rcv_view.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1));
        rcv_view.setAdapter(adapterAdminMsg);*/

    }

    // get all admin message and display
    private void AdminMsg() {

        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.loading));
        dialog.show();

        if (AppUtils.isNetworkAvailable(getApplicationContext())) {

            StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "get_admin_messages", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.e("Tag Error", response);
                    List<Admin_Msg_Model> admin_msg_models_in = new ArrayList<>();

                    try {
                        JSONObject object = new JSONObject(response);

                        if (object.getString("status").equals("true")) {

                            JSONArray array = object.getJSONArray("result");

                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object1 = array.getJSONObject(i);
                                String id = object1.getString("message_id");
                                String message = object1.getString("message");
                                String img = object1.getString("message_image");
                                String created_at = object1.getString("created_at");
                                String video = "";
                                if (object1.has("message_video")) {
                                    video = object1.getString("message_video");
                                }

                                admin_msg_models_in.add(new Admin_Msg_Model(id, message, img, video, created_at));
                            }

                            admin_msg_models = new ArrayList<>();
                            for (Admin_Msg_Model msgModel : admin_msg_models_in) {
                                boolean isFound = false;
                                for (Admin_Msg_Model model : admin_msg_models) {
                                    if (model.getId().equals(msgModel.getId())) {
                                        isFound = true;
                                        break;
                                    }
                                }
                                if (!isFound) {
                                    admin_msg_models.add(msgModel);
                                }
                            }

                            if (admin_msg_models != null && admin_msg_models.size() > 0) {

                                rcv_view.setVisibility(View.VISIBLE);
                                lin_error.setVisibility(View.GONE);

                                adapterAdminMsg = new AdapterAdminMsg(admin_msg_models, getApplicationContext());
                                rcv_view.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1));
                                rcv_view.setAdapter(adapterAdminMsg);
                                rcv_view.setFocusable(true);
                                rcv_view.setScrollX(0);
                                SharedPreferences preferences = getSharedPreferences(getString(R.string.pref_Badge), MODE_PRIVATE);
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putInt(getString(R.string.pref_admin_msg_badge_count), 0);
                                editor.apply();
                                int tot = (preferences.getInt(getString(R.string.pref_badge_count), 0) +
                                        preferences.getInt(getString(R.string.pref_admin_msg_badge_count), 0));
                                if (tot == 0) {
                                    ShortcutBadger.removeCount(getApplicationContext());
                                } else {
                                    ShortcutBadger.applyCount(getApplicationContext(), tot);
                                }

                            } else {
                                rcv_view.setVisibility(View.GONE);
                                lin_error.setVisibility(View.VISIBLE);
                                txtMsg.setText(getString(R.string.no_data_fond));
                                btnRetry.setVisibility(View.VISIBLE);
                            }

                        } else {
                            rcv_view.setVisibility(View.GONE);
                            lin_error.setVisibility(View.VISIBLE);
                            txtMsg.setText(getString(R.string.no_data_available));
                            btnRetry.setVisibility(View.VISIBLE);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        rcv_view.setVisibility(View.GONE);
                        lin_error.setVisibility(View.VISIBLE);
                        txtMsg.setText(getString(R.string.something_wrong_please));
                        btnRetry.setVisibility(View.VISIBLE);
                    }
                    dialog.dismiss();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("Tag Error", error.toString());
                    rcv_view.setVisibility(View.GONE);
                    lin_error.setVisibility(View.VISIBLE);
                    txtMsg.setText(getString(R.string.something_wrong_please));
                    btnRetry.setVisibility(View.VISIBLE);
                    dialog.dismiss();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> map = new HashMap<>();
                    map.put("towtrack_id", tow_track_id);
                    Log.e("Tag Params", map.toString());
                    return map;
                }
            };

            RequestQueue queue = Volley.newRequestQueue(this);
            request.setRetryPolicy(new DefaultRetryPolicy(3000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queue.add(request);

        } else {
            rcv_view.setVisibility(View.GONE);
            lin_error.setVisibility(View.VISIBLE);
            txtMsg.setText(getString(R.string.internet_not_connect));
            btnRetry.setVisibility(View.VISIBLE);
            dialog.dismiss();

        }
    }

    @Override
    public void onClick(View view) {

        if (view == back) {
            onBackPressed();
        } else if (view == btnRetry) {
            AdminMsg();
        } else if (view == img_refresh) {
            img_refresh.setRotation(0);
            img_refresh.animate().rotation(360).setDuration(1000).start();
            AdminMsg();
        }
    }

    class AdapterAdminMsg extends RecyclerView.Adapter<AdapterAdminMsg.ViewHolder> {

        List<Admin_Msg_Model> msg_models;
        Context context;

        public AdapterAdminMsg(List<Admin_Msg_Model> msg_models, Context context) {
            this.msg_models = msg_models;
            this.context = context;
        }

        @NonNull
        @Override
        public AdapterAdminMsg.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

            View view = LayoutInflater.from(context).inflate(R.layout.adapter_admin_msg, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final AdapterAdminMsg.ViewHolder holder, int position) {

            if (msg_models.get(position).getMsg().equals("")) {
                holder.txt_msg.setVisibility(View.GONE);
            } else {
                holder.txt_msg.setVisibility(View.VISIBLE);
                holder.txt_msg.setText(Html.fromHtml(msg_models.get(position).getMsg()));
            }

            if (msg_models.get(position).getImg().equals("")) {
                holder.img_msg.setVisibility(View.GONE);
            } else {
                holder.img_msg.setVisibility(View.VISIBLE);
                Glide.with(context).asBitmap().load(msg_models.get(position).getImg()).thumbnail(0.0f)
                        .placeholder(R.drawable.car_garage).into(new BitmapImageViewTarget(holder.img_msg) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        holder.img_msg.setImageBitmap(resource);
                    }
                });
            }

            if (!msg_models.get(position).getVideo().equals("")) {
                holder.lin_player.setVisibility(View.VISIBLE);
                holder.lin_player.removeAllViews();
                holder.lin_player.addView(VideoPlayer(getApplicationContext(), msg_models.get(position).getVideo()));
            } else {
                holder.lin_player.setVisibility(View.GONE);
                holder.lin_player.removeAllViews();
            }

            /*if (!msg_models.get(position).getVideo().equals("")){
                holder.video_play.setVisibility(View.VISIBLE);
                holder.video_play.setSource(msg_models.get(position).getVideo());
            }else {
                holder.video_play.setVisibility(View.GONE);
            }*/

            holder.txt_time.setText(AppUtils.ConvertLocalTime(msg_models.get(position).getCreated_at()));
        }

        @Override
        public int getItemCount() {
            return msg_models.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            TextView txt_time, txt_msg;
            RoundedImage img_msg;
            LinearLayout lin_player;

            public ViewHolder(@NonNull View itemView) {
                super(itemView);

                txt_msg = itemView.findViewById(R.id.txt_msg);
                img_msg = itemView.findViewById(R.id.img_msg);
                txt_time = itemView.findViewById(R.id.txt_time);
                lin_player = itemView.findViewById(R.id.lin_player);
            }
        }
    }

    public View VideoPlayer(Context context, String url) {

        AndExoPlayerView playerView = new AndExoPlayerView(context);
        playerView.setAspectRatio(EnumAspectRatio.ASPECT_16_9);
        playerView.setResizeMode(EnumResizeMode.FIT);
        playerView.setShowController(true);
        playerView.setPlayWhenReady(false);
        playerView.setSource(url);

        return playerView;
    }

    public void InitializeBroadcast() {
        LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(Act_Admin_Message.this);
        lbm.registerReceiver(receiver, new IntentFilter("send_data"));
    }

    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int make = intent.getIntExtra(getString(R.string.pref_badge_count), 0);
            int admin = intent.getIntExtra(getString(R.string.pref_admin_msg_badge_count), 0);

            if (admin != 0 && !Act_Admin_Message.this.isFinishing()){
                AdminMsg();
            }
        }
    };
}
