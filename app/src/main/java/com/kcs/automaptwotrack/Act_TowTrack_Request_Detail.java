package com.kcs.automaptwotrack;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.appcompat.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.kcs.automaptwotrack.Constant.Constant;
import com.kcs.automaptwotrack.utils.AppUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

// 0  new , 1 offered , 2 ignore, 3 user conform, 4 final confirmed, 5 cancel, 6 not selected, 7 rate user, 8 towTruck
public class Act_TowTrack_Request_Detail extends AppCompatActivity implements View.OnClickListener {

    private Toolbar toolbar;
    private TextView txt_user_location;
    private TextView txt_final_destination;
    private TextView txt_brand_name;
    private TextView txt_address;
    private TextView txt_model;
    private TextView txt_model_year;
    private TextView txt_fuel;
    private TextView txt_msg;
    private TextView txt_name;
    private TextView txt_email;
    private TextView txt_cnt;

    private CardView card_call;
    private CardView card_map;
    private CardView card_complain;
    private CardView lin_user_info;
    private Button btn_accept;
    private Button btn_reject;

    private LinearLayout lin_final_location;
    private LinearLayout lin_call;
    private LinearLayout lin_accept_reject;

    private RelativeLayout lin_main;
    private View lin_error;
    private TextView txt_error;
    private Button btn_retry;

    private SharedPreferences preferences;

    private String contact_no;
    private String tow_track_id = "";
    private String user_id = "";
    private String user_name = "";
    private String tow_track_request_id = "";

    private String user_lat;
    private String user_lng;
    private String final_lat;
    private String final_lng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_towtrack_request_detail);

        toolbar = findViewById(R.id.toolbar);
        txt_user_location = findViewById(R.id.txt_user_location);
        txt_final_destination = findViewById(R.id.txt_final_destination);
        lin_final_location = findViewById(R.id.lin_final_location);
//        lin_variant = findViewById(R.id.lin_variant);

        txt_brand_name = findViewById(R.id.txt_brand_name);
        txt_model = findViewById(R.id.txt_model);
//        txt_variant = findViewById(R.id.txt_variant);
        txt_model_year = findViewById(R.id.txt_model_year);
        txt_fuel = findViewById(R.id.txt_fuel);

        txt_email = findViewById(R.id.txt_email);
        txt_address = findViewById(R.id.txt_address);
        txt_cnt = findViewById(R.id.txt_cnt);
        txt_name = findViewById(R.id.txt_name);

        txt_msg = findViewById(R.id.txt_bottom_msg);
        card_call = findViewById(R.id.card_call);
        card_map = findViewById(R.id.card_map);
        card_complain = findViewById(R.id.card_complain);
        lin_call = findViewById(R.id.lin_call);
        lin_accept_reject = findViewById(R.id.lin_accept_reject);
        btn_accept = findViewById(R.id.btn_accept);
        btn_accept.setText(getString(R.string.offers_request));
        btn_reject = findViewById(R.id.btn_reject);
        btn_accept.setText(getString(R.string.ignor));
        lin_user_info = findViewById(R.id.lin_user_info);

        lin_main = findViewById(R.id.lin_main);
        lin_error = findViewById(R.id.lin_error);
        txt_error = findViewById(R.id.txtError);
        btn_retry = findViewById(R.id.btnRetry);

        btn_retry.setOnClickListener(this);
        card_call.setOnClickListener(this);
        card_map.setOnClickListener(this);
        btn_accept.setOnClickListener(this);
        btn_reject.setOnClickListener(this);

        getData();
        setActionBar();
        getTowTrackRequestDetail();
    }

    // get towTruck request detail
    private void getTowTrackRequestDetail() {

        final ProgressDialog dialog = new ProgressDialog(Act_TowTrack_Request_Detail.this);
        dialog.setMessage(getString(R.string.loading));
        dialog.show();

        if (AppUtils.isNetworkAvailable(this)) {

            StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "towtrack_request_detail", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("Tga Response", response);

                    dialog.dismiss();
                    try {
                        JSONObject jsonobject = new JSONObject(response);

                        if (jsonobject.getString("status").equals("true")) {

                            JSONArray array = jsonobject.getJSONArray("result");

                            for (int i=0; i<array.length(); i++){

                                JSONObject object = array.getJSONObject(i);
                                tow_track_request_id = object.getString("towtrack_request_id");
                                user_id = object.getString("user_id");
                                user_name = object.getString("name");
//                                String city = object.getString("city");
                                contact_no = object.getString("contact_no");
//                                String address = object.getString("address");
                                String email = object.getString("email");
                                String user_location = object.getString("user_location");
                                String final_destination = object.optString("final_destination", "");
                                user_lat = object.getString("user_latitude");
                                user_lng = object.getString("user_longitude");
                                final_lat = object.getString("towtrack_latitude");
                                final_lng = object.getString("towtrack_longitude");
                                String status = object.optString("status");
                                String car_brand = object.optString("brand_name", "");
                                String car_model = object.optString("model_name", "");
//                                final_lat = object.getString("final_lat");
//                                final_lng = object.optString("final_lng");
//                                String car_variant = object.getString("car_variant");
                                String model_year = object.optString("model_year", "");
                                String car_fuel = object.optString("fule_type", "");

                                lin_error.setVisibility(View.GONE);
                                lin_main.setVisibility(View.VISIBLE);

                                txt_name.setText(Html.fromHtml(user_name));
                                txt_email.setText(Html.fromHtml(email));
//                                txt_address.setText(Html.fromHtml(address));
                                txt_cnt.setText(Html.fromHtml(contact_no));

                                if (final_destination.equals("")){
                                    lin_final_location.setVisibility(View.GONE);
                                } else {
                                    lin_final_location.setVisibility(View.VISIBLE);
                                    txt_final_destination.setText(Html.fromHtml(final_destination));
                                }
                                txt_user_location.setText(Html.fromHtml(user_location));

                                txt_brand_name.setText(Html.fromHtml(car_brand));
                                txt_model.setText(Html.fromHtml(car_model));
                                txt_model_year.setText(Html.fromHtml(model_year));
                                txt_fuel.setText(Html.fromHtml(car_fuel));

                                /*if (car_variant.equals("") || car_variant.length()<1){
                                    lin_variant.setVisibility(View.GONE);
                                }else {
                                    lin_variant.setVisibility(View.VISIBLE);
                                    txt_variant.setText(Html.fromHtml(car_variant));
                                }*/

                                switch (status) {
                                    case "0":
                                        // new offers
                                        lin_user_info.setVisibility(View.GONE);
                                        lin_accept_reject.setVisibility(View.VISIBLE);
                                        break;
                                    case "1":
                                        // offered towtruck owner
                                        lin_accept_reject.setVisibility(View.GONE);
                                        txt_msg.setVisibility(View.VISIBLE);
                                        break;
                                    case "2":
                                        // ignor towtruck owner
                                        lin_accept_reject.setVisibility(View.VISIBLE);
                                        lin_user_info.setVisibility(View.GONE);
                                        btn_accept.setVisibility(View.GONE);
                                        btn_reject.setText(R.string.ignore_you);
                                        btn_reject.setEnabled(false);
                                        break;
                                    case "3":
                                    case "4":
                                        // towtruck chose for this offers
                                        lin_user_info.setVisibility(View.VISIBLE);
                                        lin_accept_reject.setVisibility(View.GONE);
                                        lin_call.setVisibility(View.VISIBLE);
                                        break;
                                    case "5":
                                        // cancel request offer
                                        lin_user_info.setVisibility(View.VISIBLE);
                                        lin_accept_reject.setVisibility(View.GONE);
                                        lin_call.setVisibility(View.GONE);
                                        card_complain.setVisibility(View.VISIBLE);
                                        break;
                                    case "7":
                                        // rate customer for this service
                                        lin_accept_reject.setVisibility(View.GONE);
                                        lin_call.setVisibility(View.GONE);
                                        txt_msg.setVisibility(View.GONE);
                                        lin_user_info.setVisibility(View.VISIBLE);
                                        RattingDialog(user_name);
                                        break;
                                    case "8":
                                        // complete service request
                                        lin_accept_reject.setVisibility(View.GONE);
                                        lin_call.setVisibility(View.GONE);
                                        break;
                                }
                            }

                        } else {
                            lin_error.setVisibility(View.VISIBLE);
                            lin_main.setVisibility(View.GONE);
                            btn_retry.setVisibility(View.GONE);
                            txt_error.setText(getString(R.string.no_data_fond));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("Tag Error", e.toString());
                        lin_error.setVisibility(View.VISIBLE);
                        lin_main.setVisibility(View.GONE);
                        btn_retry.setVisibility(View.VISIBLE);
                        txt_error.setText(getString(R.string.something_wrong_please));
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    dialog.dismiss();
                    Log.e("Tag Error", error.toString());
                    lin_error.setVisibility(View.VISIBLE);
                    lin_main.setVisibility(View.GONE);
                    btn_retry.setVisibility(View.VISIBLE);
                    txt_error.setText(getString(R.string.something_wrong_please));
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> map = new HashMap<>();
                    map.put("towtrack_request_id", tow_track_request_id);
                    map.put("towtrack_id", tow_track_id);
                    return map;
                }
            };

            RequestQueue queue = Volley.newRequestQueue(this);
            request.setRetryPolicy(new DefaultRetryPolicy(3000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queue.add(request);
        } else {

            lin_error.setVisibility(View.VISIBLE);
            lin_main.setVisibility(View.GONE);
            btn_retry.setVisibility(View.VISIBLE);
            txt_error.setText(getString(R.string.internet_not_connect));
        }
    }

    private void setActionBar() {

        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setTitle(getString(R.string.tow_truck_request_detail));
        }
    }

    private void getData() {

        preferences = getSharedPreferences(Act_Login.LOGIN_PREF, MODE_PRIVATE);

        Intent intent = getIntent();

        tow_track_request_id = intent.getStringExtra(getString(R.string.pref_request_id));

        tow_track_id = preferences.getString(getString(R.string.pref_towtrack_id), "");

        Log.e("Tag towtrack_request_id", tow_track_request_id);
        Log.e("Tag towtrack_id", tow_track_id);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return true;
    }

    @Override
    public void onClick(View view) {

        if (view == btn_retry) {
            getTowTrackRequestDetail();
        } else if (view == card_call) {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + contact_no));
            startActivity(intent);
        } else if (view == card_map) {
            Intent intent = new Intent(getApplicationContext(), Act_Map.class);
            intent.putExtra("user_lat", user_lat);
            intent.putExtra("user_lng", user_lng);
            intent.putExtra("final_lat", final_lat);
            intent.putExtra("final_lng", final_lng);
            intent.putExtra("user_name", user_name);
            startActivity(intent);
        } else if (view == btn_accept) {
            AcceptDialog(user_id, tow_track_request_id);
        } else if (view == btn_reject) {
            RejectAcceptRequest(user_id, "2", tow_track_request_id, "", "");
        } else if (view == card_complain){
            ComplainDialog();
        }
    }

    private float ratings;

    private void RattingDialog(String name) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        @SuppressLint("InflateParams")
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_accept_reject_reason, null);
        builder.setView(view);

        TextView txt_dialog_title = view.findViewById(R.id.txt_dialog_title);
        final RatingBar ratingBar = view.findViewById(R.id.ratingBar);
        Button btn_ok = view.findViewById(R.id.btn_ok);
        final Button btn_cancel = view.findViewById(R.id.btn_cancel);
        txt_dialog_title.setText("Please Rate " + name);
        btn_ok.setText(getString(R.string.yess));
        btn_cancel.setText(getString(R.string.nos));
        ratingBar.setVisibility(View.VISIBLE);
        btn_cancel.setVisibility(View.GONE);

        final AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar1, float rating, boolean fromUser) {
                ratings = rating;
                if (rating == 1) {
                    Toast.makeText(getApplicationContext(), "Bad", Toast.LENGTH_LONG).show();
                } else if (rating == 2) {
                    Toast.makeText(getApplicationContext(), "Good Bad", Toast.LENGTH_LONG).show();
                } else if (rating == 3) {
                    Toast.makeText(getApplicationContext(), "Good", Toast.LENGTH_LONG).show();
                } else if (rating == 4) {
                    Toast.makeText(getApplicationContext(), "Excellent Good ", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Excellent", Toast.LENGTH_LONG).show();
                }
            }
        });

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (ratings != 0) {
                    RattingApi("8", "" + ratings);
                } else
                    Toast.makeText(getApplicationContext(), "please select rate start", Toast.LENGTH_LONG).show();
            }
        });

    }
    private String tot_hour = "";
    private String tot_minute = "";
    @SuppressLint("ClickableViewAccessibility")
    private void AcceptDialog(final String User_Id, final String tow_track_request_id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_accept_reject_reason, null);
        builder.setView(view);
        final AlertDialog dialog = builder.create();
        dialog.show();
        TextView txt_title = view.findViewById(R.id.txt_dialog_title);
        txt_title.setText(R.string.offer_tow_truck_request);
        LinearLayout linearLayout = view.findViewById(R.id.lin_edt_price_time);
        final EditText price = view.findViewById(R.id.edt_price);
        final EditText hour = view.findViewById(R.id.edt_hour);
        Button btn_ok = view.findViewById(R.id.btn_ok);
        Button btn_cancel = view.findViewById(R.id.btn_cancel);
        btn_cancel.setVisibility(View.GONE);
        linearLayout.setVisibility(View.VISIBLE);

        Calendar calendar = Calendar.getInstance();
        final int hoursOfDay = calendar.get(Calendar.HOUR_OF_DAY);
        final int minute = calendar.get(Calendar.MINUTE);
        hour.setKeyListener(null);

        hour.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {

                    TimePickerDialog pickerDialog = new TimePickerDialog(Act_TowTrack_Request_Detail.this,android.R.style.Theme_Holo_Dialog, new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {

                            if (hourOfDay < 10 ){
                                tot_hour = "0"+hourOfDay;
                            }else {
                                tot_hour = ""+hourOfDay;
                            }

                            if (minute < 10) {
                                tot_minute = "0"+minute;
                            } else {
                                tot_minute = ""+minute;
                            }
                            hour.setText(tot_hour + ":" + tot_minute);
                        }
                    }, hoursOfDay, minute, true);

                    pickerDialog.show();
                    pickerDialog.setTitle(getString(R.string.select_arrival_total_time));
                    return true;
                }
                return false;
            }
        });

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (price.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), getString(R.string.please_enter_price), Toast.LENGTH_LONG).show();
                } else if (hour.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), getString(R.string.select_arrival_total_time), Toast.LENGTH_LONG).show();
                } else {
                    dialog.dismiss();
                    Log.e("TAG", User_Id);
                    Log.e("TAG", tow_track_request_id);
                    Log.e("TAG", price.getText().toString());
                    Log.e("TAG", hour.getText().toString());
                    RejectAcceptRequest(User_Id, "1", tow_track_request_id, price.getText().toString(), hour.getText().toString());
                }
            }
        });

    }

    // request offered and ignore api
    private void RejectAcceptRequest(final String _user_id, final String _status, final String towtrack_request_id,
                                     final String price, final String time) {

        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.loading));
        dialog.show();

        Log.e("TAG", _user_id);
        Log.e("TAG", _status);
        Log.e("TAG", price);
        Log.e("TAG", time);
        Log.e("TAG", towtrack_request_id);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL + "response_towtrack_request",
                new Response.Listener<String>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(String response) {
                        Log.e("response_garage", " " + response);
                        try {
                            JSONObject mainJsonObject = new JSONObject(response);
                            if (mainJsonObject.optString("status").equals("1")) {
                                getTowTrackRequestDetail();
                                showMessage(mainJsonObject.getString("message"));
                            } else {
                                showMessage(mainJsonObject.getString("message"));
//                                Toast.makeText(getApplicationContext(), "try again", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        dialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurs
                        Log.e("TAG", error.toString());
                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            //This indicates that the request has either time out or there is no connection
                            Toast.makeText(getApplicationContext(), getString(R.string.internet_not_connect), Toast.LENGTH_LONG).show();
                        } else if (error instanceof AuthFailureError) {
                            // Error indicating that there was an Authentication Failure while performing the request
                        } else if (error instanceof ServerError) {
                            //Indicates that the server responded with a error response
                        } else if (error instanceof NetworkError) {
                            //Indicates that there was network error while performing the request
                        } else if (error instanceof ParseError) {
                            // Indicates that the server response could not be parsed
                        }
                        dialog.dismiss();
                    }
                }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", _user_id);
                params.put("towtrack_id", tow_track_id);
                params.put("status", _status);
                params.put("towtrack_request_id", towtrack_request_id);
                if (_status.equals("1")) {
                    params.put("price", price);
                    params.put("time", time);
                }
                Log.e("params", " : " + params);

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void showMessage(String msg){

        AlertDialog.Builder builder = new AlertDialog.Builder(Act_TowTrack_Request_Detail.this);
        builder.setMessage(msg);
        builder.setPositiveButton(getText(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                onBackPressed();
            }
        });

        AlertDialog dialog = builder.create();
        setFinishOnTouchOutside(false);
        dialog.show();
    }

    // ratinf custome api
    private void RattingApi(final String status, final String rate) {
        final ProgressDialog dialogs = new ProgressDialog(this);
        dialogs.show();

        StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "towtrack_user_ratting",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Cancel Order response", response);
                        dialogs.dismiss();
                        if (response != null) {
                            try {
                                JSONObject object = new JSONObject(response);
                                if (object.getString("status").equals("1")) {
                                    onBackPressed();
                                } else {
                                    Toast.makeText(getApplicationContext(), getString(R.string.try_again_after_some_time), Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Tag", error.toString());
                dialogs.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> map = new HashMap<>();
                map.put("status", status);
                map.put("towtrack_id", tow_track_id);
                map.put("towtrack_request_id", tow_track_request_id);
                map.put("user_id", user_id);
                map.put("ratting", rate);
                Log.e("Tag Params", map.toString());
                return map;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(3000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
    }

    public void ComplainDialog() {

        AlertDialog.Builder dialog = new AlertDialog.Builder(Act_TowTrack_Request_Detail.this);

        View view = LayoutInflater.from(Act_TowTrack_Request_Detail.this).inflate(R.layout.dialog_complain, null, false);
        dialog.setView(view);
        final AlertDialog dialog1 = dialog.create();
        dialog1.show();

        final EditText ed_complain = view.findViewById(R.id.ed_complain);
        Button button = view.findViewById(R.id.btn_complain);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ed_complain.getText().toString().equals("")) {
                    ed_complain.requestFocus();
                    ed_complain.setError("Enter Detail");
                } else {
                    StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "towtrack_complain", new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e("Tag Response", response);

                            try {
                                JSONObject object = new JSONObject(response);
                                if (object.getString("status").equals("true")) {
                                    dialog1.dismiss();
                                    Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_LONG).show();
                                    finish();
                                } else {
                                    Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_LONG).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("Tag error", error.toString());
                            Toast.makeText(getApplicationContext(), getString(R.string.something_wrong_please), Toast.LENGTH_LONG).show();
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> map = new HashMap<>();
                            map.put("towtrack_id", tow_track_id);
                            map.put("user_id",user_id);
                            map.put("status","10");
                            map.put("towtrack_request_id",tow_track_request_id);
                            map.put("complain", ed_complain.getText().toString());
                            Log.e("Tag Params", map.toString());
                            return map;
                        }
                    };

                    RequestQueue queue = Volley.newRequestQueue(Act_TowTrack_Request_Detail.this);
                    queue.add(request);
                }
            }
        });
    }
}
