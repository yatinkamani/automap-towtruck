package com.kcs.automaptwotrack;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.kcs.automaptwotrack.Constant.Constant;
import com.kcs.automaptwotrack.Model.City_Data_Model;
import com.kcs.automaptwotrack.Model.Contry_spinner_model;
import com.kcs.automaptwotrack.Model.Edit_User_Model;
import com.kcs.automaptwotrack.Model.VolleyMultipartRequest;
import com.kcs.automaptwotrack.newPackage.Helper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.kcs.automaptwotrack.Act_Login.LOGIN_PREF;
import static com.kcs.automaptwotrack.Act_splash_screen.REQUEST_ID_MULTIPLE_PERMISSIONS;

public class Act_Edit_Profile_Detail extends AppCompatActivity {

    EditText e_nm, e_email, e_country, e_con_no, ed_description, ed_password, ed_address;
    TextView txt_E_address, txt_E_nm;
    int pos;

    CardView c_edit;

    TextView tv_lat, tv_long;
    private Bitmap bitmap;
    File image_file;
    ImageView banner_image;

    String towTrack_id, name, email_id, country_id, city_id, address, con_no, description, password;

    ImageView ed_pro_back;

    Edit_User_Model ed_model = new Edit_User_Model();

    SharedPreferences prefs;
    TextView txt_upload_banner;

    Spinner spinner_country, spinner_city;
    private ArrayList<Contry_spinner_model> country_data_models;
    private ArrayList<City_Data_Model> city_data_models;
    private ArrayList<String> names = new ArrayList<String>();
    private ArrayList<String> city_id_array = new ArrayList<String>();

    ScrollView scrollView;

    int PICK_IMAGE_CAMERA1 = 200, PICK_IMAGE_GALLERY1 = 201;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_act__edit__profile__detail);
        Intent intent = getIntent();

        BindView();

        if (Constant.i == 1) {
            towTrack_id = prefs.getString(getString(R.string.pref_towtrack_id), "");
        } else {
            towTrack_id = intent.getStringExtra(getString(R.string.pref_towtrack_id));
        }
        TowTrackStatus(towTrack_id);

        banner_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });

        c_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadBitmap(bitmap);
            }
        });
        SpinnerModel();

        scrollView = findViewById(R.id.scrollView);
    }

    public void BindView(){
        ed_pro_back = (ImageView) findViewById(R.id.ed_pro_back);
        ed_pro_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        if (getIntent().getBooleanExtra("SPLASH", false)) {
            ed_pro_back.setVisibility(View.INVISIBLE);
        }

        banner_image = findViewById(R.id.banner_image);
        e_nm = findViewById(R.id.ed_E_nm);
        e_email = findViewById(R.id.ed_E_email);
        e_country = findViewById(R.id.ed_E_country);
        e_con_no = findViewById(R.id.ed_E_contact);
        ed_description = findViewById(R.id.ed_description);
        ed_password = findViewById(R.id.ed_password);
        c_edit = findViewById(R.id.edit_E_card);
        ed_address = findViewById(R.id.ed_address);
        txt_upload_banner = findViewById(R.id.txt_upload_banner);

        txt_E_address = findViewById(R.id.txt_E_address);
        txt_E_nm = findViewById(R.id.txt_E_nm);

        spinner_country = (Spinner) findViewById(R.id.Sp_select_country);
        spinner_city = (Spinner) findViewById(R.id.Sp_select_city);
        tv_lat = findViewById(R.id.tv_let);
        tv_long = findViewById(R.id.tv_long);

        name = e_nm.getText().toString();

        prefs = getSharedPreferences(LOGIN_PREF, Context.MODE_PRIVATE);
        name = prefs.getString(getString(R.string.pref_towtrack_name), "");
        email_id = prefs.getString(getString(R.string.pref_email), "");

        con_no = prefs.getString(getString(R.string.pref_contact_no), "");
        country_id = prefs.getString(getString(R.string.pref_country_id), "");
        city_id = prefs.getString(getString(R.string.pref_spinner_city),"");
    }

    public void SpinnerModel() {

        // country select spinner and selected id show city spinner
        spinner_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                pos = i;
                country_id = country_data_models.get(i).getCon_id();
                SharedPreferences.Editor prefEditor = prefs.edit();
                prefEditor.putString(getString(R.string.pref_spinner_country), spinner_country.getSelectedItem().toString());
                prefEditor.apply();

                String city = prefs.getString(getString(R.string.pref_spinner_city), " ");
                ArrayList<String> list = new ArrayList<>();
                city_id_array = new ArrayList<>();
                if (city_data_models != null && city_data_models.size()>0){
                    for (int j=0; j<city_data_models.size(); j++) {
                        if (city_data_models.get(j).getCon_id().equals(country_id)) {
                            list.add(city_data_models.get(j).getCity_name());
                            city_id_array.add(city_data_models.get(j).getCity_id());
                        }
                    }
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(Act_Edit_Profile_Detail.this, android.R.layout.simple_spinner_dropdown_item, list);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner_city.setAdapter(adapter);

                for (int p=0; p<list.size(); p++){
                    if (city_id.equals(city_id_array.get(p))){
                        spinner_city.setSelection(p);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // DO Nothing here
            }
        });

        // City select spinner
        spinner_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                city_id = city_id_array.get(i);
//                city_name = adapterView.getItemAtPosition(i).toString();
                SharedPreferences.Editor prefEditor = prefs.edit();
                prefEditor.putString(getString(R.string.pref_spinner_city), spinner_city.getSelectedItem().toString());
                prefEditor.apply();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private boolean hasImage(@NonNull ImageView view) {
        Drawable drawable = view.getDrawable();
        boolean hasImage = (drawable != null);

        if (hasImage && (drawable instanceof BitmapDrawable)) {
            hasImage = ((BitmapDrawable) drawable).getBitmap() != null;
        }

        return hasImage;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_GALLERY1 && resultCode == RESULT_OK && data != null) {
            //getting the image Uri
            Uri imageUri = data.getData();
            try {
                //  getting bitmap object from uri
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                //  displaying selected image to ImageView
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(imageUri, filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                cursor.close();
                image_file = new File(picturePath);
                banner_image.setImageBitmap(bitmap);
                txt_upload_banner.setVisibility(View.GONE);
                //  calling the method uploadBitmap to upload image
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == PICK_IMAGE_CAMERA1 && resultCode == RESULT_OK && data != null) {
            File destination;
            try {
                Uri selectedImage = data.getData();
                bitmap = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);

                Log.e("Activity", "Pick from Camera::>>> ");

                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
                destination = new File(Environment.getExternalStorageDirectory() + "/" + getString(R.string.app_name), "IMG_" + timeStamp + ".jpg");
                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                image_file = new File(destination.getAbsolutePath());
                banner_image.setImageBitmap(bitmap);
                txt_upload_banner.setVisibility(View.GONE);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    ProgressDialog progress;

    // upload towtruck detail with banner image
    private void uploadBitmap(final Bitmap bitmap) {

        name = e_nm.getText().toString();
        email_id = e_email.getText().toString();
        address = ed_address.getText().toString();
        con_no = e_con_no.getText().toString();
        description = ed_description.getText().toString();
        password = ed_password.getText().toString();

        if (e_nm.getVisibility() == View.VISIBLE && TextUtils.isEmpty(name)) {
            e_nm.setError(getString(R.string.name));
        } /*else if (TextUtils.isEmpty(email_id)) {
            e_email.setError("enter email");
        }*/ /*else if (TextUtils.isEmpty(state)) {
            e_state.setError("enter state");
        } else if (TextUtils.isEmpty(city)) {
            e_city.setError("enter city");
        }*/else if (TextUtils.isEmpty(address)) {
            ed_address.setError("enter address");
        } /*else if (TextUtils.isEmpty(description)) {
            ed_description.setError("Enter description");
            ed_description.requestFocus();
        } */else if (TextUtils.isEmpty(password)) {
            ed_password.setError(getString(R.string.password));
            ed_password.requestFocus();
        }/* else if (TextUtils.isEmpty(pin)) {
            e_pin.setError("Enter pin");
        } else if (TextUtils.isEmpty(con_no)) {
            e_con_no.setError("Enter contact no");
        }*/else if (!hasImage(banner_image)) {
            Toast.makeText(Act_Edit_Profile_Detail.this, getString(R.string.select_image), Toast.LENGTH_SHORT).show();
        } else if (!Helper.isNetworkAvailable(getApplicationContext())) {
            Toast.makeText(Act_Edit_Profile_Detail.this, R.string.internet_not_connect, Toast.LENGTH_SHORT).show();
        } else {

            progress = new ProgressDialog(Act_Edit_Profile_Detail.this);
            progress.setMessage(getString(R.string.please_wait_update_profile));
            progress.setCancelable(false);
            progress.show();

            VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, Constant.URL + "towtrack_profile_update",
                    new Response.Listener<NetworkResponse>() {
                        @Override
                        public void onResponse(NetworkResponse response) {
                            progress.dismiss();
                            String responses = Html.fromHtml(new String(response.data)).toString();
                            responses = responses.substring(responses.indexOf("{"),responses.lastIndexOf("}") + 1);

                            Log.e("response", " " + new String(responses));
                            Log.e("response data", " " + new String(response.data));
                            try {
                                JSONObject obj = new JSONObject(new String(responses));

                                ed_model.setMessage(obj.getString("message"));
//                                Toast.makeText(Act_Edit_Profile_Detail.this, ed_model.getMessage(), Toast.LENGTH_SHORT).show();

                                if (obj.optString("status").equals("1")) {

                                    SharedPreferences.Editor editor = prefs.edit();
                                    editor.putString(getString(R.string.pref_towtrack_name),name);
                                    editor.putString(getString(R.string.pref_twotrack_image), ""+image_file);
                                    editor.putString(getString(R.string.pref_spinner_city),city_id);
                                    editor.putString(getString(R.string.pref_country_id),country_id);
                                    editor.apply();

//                                    Toast.makeText(Act_Edit_Profile_Detail.this, ed_model.getMessage(), Toast.LENGTH_SHORT).show();
                                    Toast.makeText(Act_Edit_Profile_Detail.this, getString(R.string.profile_update), Toast.LENGTH_SHORT).show();
                                    Log.e("ppp", "onResponse: " + ed_model.getMessage());

                                    Intent intent = new Intent(Act_Edit_Profile_Detail.this, Navigation_Activity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            } catch (JSONException e) {
                                Toast.makeText(Act_Edit_Profile_Detail.this, getString(R.string.something_wrong_please), Toast.LENGTH_SHORT).show();
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progress.dismiss();
                            Log.e("volley_error", " " + error.getMessage());

                            Toast.makeText(Act_Edit_Profile_Detail.this, getString(R.string.something_wrong_please), Toast.LENGTH_SHORT).show();

                        }
                    }) {

                /*
                 * If you want to add more parameters with the image
                 * you can do it here
                 * here we have only one parameter with the image
                 * which is tags
                 */

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("towtrack_id", towTrack_id);
                    params.put("email", email_id);
                    params.put("password", password);
                    params.put("description", description);
                    params.put("towtrack_name", name);
                    params.put("country_id", country_id);
                    params.put("contact_no", con_no);
                    params.put("city_id", city_id);
                    params.put("address", address);
                    Log.e("params", " " + params);

                    return params;
                }

                /*
                 * Here we are passing image by renaming it with a unique name
                 */

                @Override
                protected Map<String, DataPart> getByteData() throws AuthFailureError {

                    Map<String, DataPart> params = new HashMap<>();
                    if (bitmap != null) {
                        long image_name = System.currentTimeMillis();
                        params.put("towtrack_profile_img", new DataPart(image_name + ".png", getFileDataFromDrawable(bitmap)));
                        return params;
                    }
                    return super.getByteData();
                }
            };

            Volley.newRequestQueue(this).add(volleyMultipartRequest);
            volleyMultipartRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 100000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 100000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
        }
    }

    // get towtruck deatil
    private void TowTrackStatus(final String TowTrack_id) {
        progress = new ProgressDialog(this);
        progress.setMessage(getString(R.string.loading));
        progress.show();
        RequestQueue queue = Volley.newRequestQueue(Act_Edit_Profile_Detail.this);
        StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "check_towtrack_status", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("OnResponse", "onResponse: " + response);
                String tow_track_name = "";
                String email = "";
                String contact_no = "";
                String description = "";
                String address_ = "";
                String tow_track_image = "";
                String currency = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("status").equals("true")) {
//                        JSONArray jsonArray = object.getJSONArray("result");
                        JSONArray jsonArray = object.getJSONArray("result");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject object1 = jsonArray.getJSONObject(i);
                            tow_track_name = object1.getString("towtrack_name");
                            email = object1.getString("email");
                            contact_no = object1.getString("contact_no");
                            description = object1.getString("description");
//                            state = object1.getString("state");

                            city_id = object1.optString("city_id","");
                            country_id = object1.getString("country_id");
                            address_ = object1.getString("address");
//                            pinCode = object1.getString("pinCode");
                            tow_track_image = object1.getString("towtrack_profile_img");

                            JSONObject country_data = object1.optJSONObject("country_data");

                            Log.e("ppp_ppp", "onResponse: " + response);

                        }

                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString(getString(R.string.pref_curency), currency);
                        editor.putString(getString(R.string.pref_twotrack_image), tow_track_image);
                        editor.apply();

                        if (!tow_track_name.equals("null"))
                            e_nm.setText(tow_track_name);
                        if (!email.equals("null"))
                            e_email.setText(email);
                        if (!contact_no.equals("null"))
                            e_con_no.setText(contact_no);

                        if (!description.equals("null"))
                            ed_description.setText(description);

                        /*if (!state.equals("null"))
                            e_state.setText(state);
                        if (!city.equals("null"))
                            e_city.setText(city);
                        if (!pincode.equals("null"))
                            e_pin.setText(pincode);*/

                        if (!address_.equals("null")) {
                           ed_address.setText(address_);
                        }

                        if (!tow_track_image.toString().equals("null") && tow_track_image.length() > 0) {
                            txt_upload_banner.setVisibility(View.GONE);
                            Glide.with(getApplicationContext()).load(tow_track_image).listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    txt_upload_banner.setVisibility(View.VISIBLE);
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    banner_image.setImageDrawable(resource);
                                    txt_upload_banner.setVisibility(View.GONE);
                                    return true;
                                }
                            }).into(banner_image);
                        } else {
                            banner_image.setImageDrawable(null);
                            txt_upload_banner.setVisibility(View.VISIBLE);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                retrieveJSON2();
                progress.dismiss();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                retrieveJSON2();
                Log.e("ppp_ppp", "onErrorResponse: " + error);
                progress.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("towtrack_id", TowTrack_id);
                return map;
            }
        };
        queue.add(request);
    }

    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    // get country and city data and set spinner and set default selected
    private void retrieveJSON2() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constant.URL + "get_country",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject obj = new JSONObject(response);

                            if (obj.optString("status").equals("true")) {

                                country_data_models = new ArrayList<>();
                                city_data_models = new ArrayList<>();
                                JSONObject object = obj.getJSONObject("result");
                                JSONArray dataArray = object.getJSONArray("country");

                                for (int i = 0; i < dataArray.length(); i++) {

                                    Contry_spinner_model playerModel = new Contry_spinner_model();
                                    JSONObject dataObj = dataArray.getJSONObject(i);
                                    playerModel.setCon_id(dataObj.getString("country_id"));
                                    playerModel.setCon_code(dataObj.getString("country_code"));
                                    playerModel.setCon_name(dataObj.getString("country_name"));
                                    playerModel.setCon_currency(dataObj.getString("currency"));
                                    playerModel.setCon_language(dataObj.getString("language"));
                                    playerModel.setCon_ph_code(dataObj.getString("phone_code"));
                                    country_data_models.add(playerModel);

                                }

                                JSONArray array = object.getJSONArray("city");

                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject object1 = array.getJSONObject(i);
                                    City_Data_Model city_data_model = new City_Data_Model();

                                    city_data_model.setCity_id(object1.getString("city_id"));
                                    city_data_model.setCity_name(object1.getString("city_name"));
                                    city_data_model.setCon_id(object1.getString("country_id"));
                                    city_data_models.add(city_data_model);
                                }

                                for (int i = 0; i < country_data_models.size(); i++) {
                                    names.add(country_data_models.get(i).getCon_name().toString());
                                }
                                String country = prefs.getString(getString(R.string.pref_spinner_country), " ");

                                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(Act_Edit_Profile_Detail.this, android.R.layout.simple_spinner_dropdown_item, names);
                                spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                                spinner_country.setAdapter(spinnerArrayAdapter);

                                for (int i = 0; i < country_data_models.size(); i++) {
                                    if (country.equals(spinner_country.getItemAtPosition(i).toString())) {
                                        spinner_country.setSelection(i);
                                        break;
                                    }

                                    if (country_id != null){
                                        if (country_id.equals(country_data_models.get(i).getCon_id())){
                                            spinner_country.setSelection(i);
                                        }
                                    }
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  displaying the error in toast if occurs
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

//         request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        requestQueue.add(stringRequest);

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void selectImage() {

        try {
            PackageManager pm = getPackageManager();
            int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, getPackageName());
            if (hasPerm == PackageManager.PERMISSION_GRANTED) {
                final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Cancel"};
                AlertDialog.Builder builder = new AlertDialog.Builder(Act_Edit_Profile_Detail.this);
                builder.setTitle("Select Option");
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (options[item].equals("Take Photo")) {
                            dialog.dismiss();
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(intent, PICK_IMAGE_CAMERA1);
                        } else if (options[item].equals("Choose From Gallery")) {
                            dialog.dismiss();
                            Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY1);
                        } else if (options[item].equals("Cancel")) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            } else{
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, REQUEST_ID_MULTIPLE_PERMISSIONS);
                Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, REQUEST_ID_MULTIPLE_PERMISSIONS);
            Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }
}
