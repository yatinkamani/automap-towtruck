package com.kcs.automaptwotrack.Model;

public class Request_Model {

    private String tow_track_request_id, user_id, tow_track_id, user_name, status, email, city, address, pincode, user_profile_image,
            contact_no, state, final_lat, final_log, lat, log, user_location, final_destination, car_brand, car_model, car_model_year,
            car_variant, car_fuel, time_out_status;

    public Request_Model(String tow_track_request_id, String user_id, String tow_track_id, String user_name, String status,
                         String email, String city, String address, String pincode, String user_profile_image, String contact_no,
                         String state, String final_lat, String final_log, String lat, String log, String user_location,
                         String final_destination, String car_brand, String car_model, String car_model_year, String car_variant,
                         String car_fuel, String time_out_status) {
        this.tow_track_request_id = tow_track_request_id;
        this.user_id = user_id;
        this.tow_track_id = tow_track_id;
        this.user_name = user_name;
        this.status = status;
        this.email = email;
        this.city = city;
        this.address = address;
        this.pincode = pincode;
        this.user_profile_image = user_profile_image;
        this.contact_no = contact_no;
        this.state = state;
        this.final_lat = final_lat;
        this.final_log = final_log;
        this.lat = lat;
        this.log = log;
        this.user_location = user_location;
        this.final_destination = final_destination;
        this.car_brand = car_brand;
        this.car_model = car_model;
        this.car_model_year = car_model_year;
        this.car_variant = car_variant;
        this.car_fuel = car_fuel;
        this.time_out_status = time_out_status;
    }

    public String getTime_out_status() {
        return time_out_status;
    }

    public void setTime_out_status(String time_out_status) {
        this.time_out_status = time_out_status;
    }

    public String getTow_track_request_id() {
        return tow_track_request_id;
    }

    public void setTow_track_request_id(String tow_track_request_id) {
        this.tow_track_request_id = tow_track_request_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getTow_track_id() {
        return tow_track_id;
    }

    public void setTow_track_id(String tow_track_id) {
        this.tow_track_id = tow_track_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getUser_profile_image() {
        return user_profile_image;
    }

    public void setUser_profile_image(String user_profile_image) {
        this.user_profile_image = user_profile_image;
    }

    public String getContact_no() {
        return contact_no;

    }

    public void setContact_no(String contact_no) {
        this.contact_no = contact_no;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getFinal_lat() {
        return final_lat;
    }

    public void setFinal_lat(String final_lat) {
        this.final_lat = final_lat;
    }

    public String getFinal_log() {
        return final_log;
    }

    public void setFinal_log(String final_log) {
        this.final_log = final_log;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }

    public String getUser_location() {
        return user_location;
    }

    public void setUser_location(String user_location) {
        this.user_location = user_location;
    }

    public String getFinal_destination() {
        return final_destination;
    }

    public void setFinal_destination(String final_destination) {
        this.final_destination = final_destination;
    }

    public String getCar_brand() {
        return car_brand;
    }

    public void setCar_brand(String car_brand) {
        this.car_brand = car_brand;
    }

    public String getCar_model() {
        return car_model;
    }

    public void setCar_model(String car_model) {
        this.car_model = car_model;
    }

    public String getCar_model_year() {
        return car_model_year;
    }

    public void setCar_model_year(String car_model_year) {
        this.car_model_year = car_model_year;
    }

    public String getCar_variant() {
        return car_variant;
    }

    public void setCar_variant(String car_variant) {
        this.car_variant = car_variant;
    }

    public String getCar_fuel() {
        return car_fuel;
    }

    public void setCar_fuel(String car_fuel) {
        this.car_fuel = car_fuel;
    }

}
