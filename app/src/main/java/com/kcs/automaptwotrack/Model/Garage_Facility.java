package com.kcs.automaptwotrack.Model;

public class Garage_Facility {

    private String facility_id,facility_name,status,created_at,update_at;

    public Garage_Facility(String facility_id, String facility_name, String status, String created_at, String update_at) {
        this.facility_id = facility_id;
        this.facility_name = facility_name;
        this.status = status;
        this.created_at = created_at;
        this.update_at = update_at;
    }

    public Garage_Facility() {}

    public String getFacility_id() {
        return facility_id;
    }

    public void setFacility_id(String facility_id) {
        this.facility_id = facility_id;
    }

    public String getFacility_name() {
        return facility_name;
    }

    public void setFacility_name(String facility_name) {
        this.facility_name = facility_name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdate_at() {
        return update_at;
    }

    public void setUpdate_at(String update_at) {
        this.update_at = update_at;
    }
}
