package com.kcs.automaptwotrack.Model;

public class Check_user_Model {

    private String request, message, userid, name, email, contact_no, country_id, state,
            city, address, pincode, user_image, status, registration_date, updated_at;

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContact_no() {
        return contact_no;
    }

    public void setContact_no(String contact_no) {
        this.contact_no = contact_no;
    }

    public String getCountry_id() {
        return country_id;
    }

    public void setCountry_id(String country_id) {
        this.country_id = country_id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRegistration_date() {
        return registration_date;
    }

    public void setRegistration_date(String registration_date) {
        this.registration_date = registration_date;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public Check_user_Model(String request, String message, String userid, String name, String email,
                            String contact_no, String country_id, String state, String city, String address,
                            String pincode, String user_image, String status, String registration_date, String updated_at) {
        this.request = request;
        this.message = message;
        this.userid = userid;
        this.name = name;
        this.email = email;
        this.contact_no = contact_no;
        this.country_id = country_id;
        this.state = state;
        this.city = city;
        this.address = address;
        this.pincode = pincode;
        this.user_image = user_image;
        this.status = status;
        this.registration_date = registration_date;
        this.updated_at = updated_at;
    }

    public Check_user_Model() {
    }
}
