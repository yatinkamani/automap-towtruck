package com.kcs.automaptwotrack.Model;

public class Forgot_model {

    String status;
    String message;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    String user_id;
    String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Forgot_model() { }

    public Forgot_model(String status, String message, String user_id, String code) {
        this.status = status;
        this.message = message;
        this.user_id = user_id;
        this.code = code;
    }
}
