package com.kcs.automaptwotrack;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.kcs.automaptwotrack.Constant.Constant;
import com.kcs.automaptwotrack.Model.Active_user_model;
import com.kcs.automaptwotrack.Model.Check_user_Model;
import com.kcs.automaptwotrack.Model.Login_Model;
import com.kcs.automaptwotrack.newPackage.Helper;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Act_Login extends AppCompatActivity {

    public static final String LOGIN_PREF = "Login_preference" ;
    private SharedPreferences sharedpreferences;
    private SharedPreferences.Editor editor;

    //  Button btn_login, btn_forgot, btn_fb, btn_google;

    private TextView btn_new_user;

    private TextView btn_login;
    private TextView forgot;
    private EditText ed_contact;
    private EditText ed_pass;

    private String st_contact;
    private String st_pass;

    private ArrayList<Login_Model> loginModel = new ArrayList<>();
    private ArrayList<Active_user_model> Active_user_model;
    private final Active_user_model active_user_model = new Active_user_model();
    private ArrayList<Check_user_Model> check_model;

    private final Check_user_Model playerModel = new Check_user_Model();
    private ProgressDialog progressDialog;

    private final Login_Model login_model = new Login_Model();
    private ImageView login_back;
    private String DEVICE_TOKEN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__login);

        FirebaseApp.initializeApp(this);
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            return;
                        }
                        DEVICE_TOKEN = task.getResult().getToken();
                        Log.d("DEVICE_TOKEN", DEVICE_TOKEN);
                        //  "fihfQBbZf7M:APA91bEDndPUJIwb-naFmnlkF0EhTd8iFH3cehat2Dwo67j0UZ7NyarRT-RFcoH2ifnA5EDjAtImN6rPvmULC9dHq_kzmDMG8L0SSSK4g3vuTxKGRlluEKpMNZGt0lVtMIK7xJYYzqgY"
                    }
                });

        login_back = findViewById(R.id.login_back);
        login_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        sharedpreferences = getSharedPreferences(LOGIN_PREF, Context.MODE_PRIVATE);
        Intent intent = getIntent();

        onNewIntent(intent);

        btn_login = findViewById(R.id.btn_Login);
        btn_new_user = findViewById(R.id.btn_new_user);
        forgot = findViewById(R.id.TV_forgot);
        ed_contact = findViewById(R.id.Ed_L_uid);
        ed_pass = findViewById(R.id.Ed_L_pwd);

        forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(Act_Login.this, Act_forgot_password.class);
                startActivity(intent1);
            }
        });

        // login with validation check
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog = new ProgressDialog(Act_Login.this);
                st_contact = ed_contact.getText().toString();
                st_pass = ed_pass.getText().toString();

                if (TextUtils.isEmpty(st_contact)) {
                    ed_contact.setError(getString(R.string.contact_no));
                } else if (st_contact.length() < 6 || st_contact.length() > 15) {
                    ed_contact.setError(getString(R.string.enter_valid_contact));
                } else if (TextUtils.isEmpty(st_pass)) {
                    ed_pass.setError(getString(R.string.password));
                } else if(!Helper.isNetworkAvailable(getApplicationContext())){
                    Toast.makeText(getApplicationContext(),R.string.internet_not_connect,Toast.LENGTH_LONG).show();
                } else {
                    RequestQueue queue = Volley.newRequestQueue(Act_Login.this);
                    progressDialog.setMessage(getString(R.string.loading));
                    progressDialog.show();
                    StringRequest request = new StringRequest(Request.Method.POST, Constant.URL+"towtrack_login", new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e("response"," " +response);
                            /*  progressDialog.dismiss(); */
                            try {
                                JSONObject obj = new JSONObject(response);
                                loginModel = new ArrayList<>();

                                if (obj.optString("status").equals("true")) {

                                    login_model.setStatus_up(obj.getString("status"));
                                    login_model.setMessage(obj.getString("message"));

                                    JSONArray dataArray = obj.getJSONArray("result");

                                    for (int i = 0; i < dataArray.length(); i++) {
                                        JSONObject data_obj = dataArray.getJSONObject(i);
                                        login_model.setUser_id(data_obj.getString("towtrack_id"));
                                        login_model.setName(data_obj.getString("towtrack_name"));
                                        login_model.setEmail(data_obj.getString("email"));
                                        login_model.setContact_no(data_obj.getString("contact_no"));
                                        login_model.setCountry_id(data_obj.getString("country_id"));
//                                        login_model.setState(data_obj.getString("state"));
                                        login_model.setCity(data_obj.getString("city_id"));
                                        login_model.setAddress(data_obj.getString("address"));
//                                        login_model.setPin(data_obj.getString("pincode"));
                                        login_model.setStatus(data_obj.getString("status"));
                                        login_model.setUpdate_date(data_obj.getString("updated_at"));
                                        loginModel.add(login_model);
                                    }
                                    Log.e("twotrack_id", login_model.getUser_id());
                                    Log.e("Towtrack name", login_model.getName());
                                    Log.e("email_id", login_model.getEmail());

                                    SharedPreferences.Editor editor = sharedpreferences.edit();
                                    editor.putString(getString(R.string.pref_towtrack_id), login_model.getUser_id());
                                    editor.putString(getString(R.string.pref_password), st_pass);

                                    editor.putString(getString(R.string.pref_towtrack_name), login_model.getName());
                                    editor.putString(getString(R.string.pref_email), login_model.getEmail());
                                    editor.putString(getString(R.string.pref_contact_no), login_model.getContact_no());
                                    editor.putString(getString(R.string.pref_country_id), login_model.getCountry_id());
                                    editor.putString(getString(R.string.pref_spinner_city), login_model.getCity());
                                    editor.apply();

                                    if (obj.optString("status").equals("true")) {
                                        // Toast.makeText(Act_Login.this, login_model.getUser_id(), Toast.LENGTH_SHORT).show();
                                        retrieveJSON(login_model.getUser_id());
                                    }else {
                                        progressDialog.dismiss();
                                    }
//                                    Toast.makeText(Act_Login.this, login_model.getMessage(), Toast.LENGTH_SHORT).show();
                                    Toast.makeText(Act_Login.this, getString(R.string.verification_successfuly), Toast.LENGTH_SHORT).show();
                                }else {
                                    progressDialog.dismiss();
                                    Toast.makeText(Act_Login.this, obj.getString("message"), Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.dismiss();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.hide();
                            //  Toast.makeText(Act_Login.this, "my error :" + error, Toast.LENGTH_LONG).show();
                            Toast.makeText(getApplicationContext(),getString(R.string.something_wrong_please),Toast.LENGTH_LONG).show();
                            Log.i("My error", "" + error);
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() {

                            Map<String, String> map = new HashMap<>();
//                            map.put("email", st_contact);
                            map.put("contact_no", st_contact);
                            map.put("password", st_pass);
                            map.put("device_token", DEVICE_TOKEN);
                            String language = "";
                            if (sharedpreferences.getString(getString(R.string.pref_language),"").equals("ar")){
                                language = "Arabic";
                            }else {
                                language = "English";
                            }
                            map.put("language",language);
                            Log.e("params"," "+map);
                            return map;
                        }
                    };
                    queue.add(request);
                }
            }
        });

        btn_new_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i_new = new Intent(Act_Login.this, Act_Registration.class);
                startActivity(i_new);
            }
        });
    }

    public static boolean isValidEmail(CharSequence target) {
        return (Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    private void active(final String id, final String type) {
        RequestQueue queue = Volley.newRequestQueue(Act_Login.this);
        StringRequest request = new StringRequest(Request.Method.POST, Constant.URL+"active_account", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject obj = new JSONObject(response);
                    obj.getString("message");
                    String msg = obj.getString("message");
                    active_user_model.setMessage(obj.getString("message"));
                    Toast.makeText(Act_Login.this, msg, Toast.LENGTH_SHORT).show();
                    Active_user_model = new ArrayList<>();
                    Active_user_model.add(active_user_model);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(Act_Login.this, "my error :" + error, Toast.LENGTH_LONG).show();
                Log.i("My error", "" + error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> map = new HashMap<>();
                map.put("id", id);
                map.put("type", type);
                Log.e("test2"," " +map.toString());

                return map;
            }
        };
        queue.add(request);
    }

    // get towtruck data and redirect screen base on status
    private void retrieveJSON(final String id) {
        RequestQueue queue = Volley.newRequestQueue(Act_Login.this);
        StringRequest request = new StringRequest(Request.Method.POST, Constant.URL+"check_towtrack_status", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    Log.e("check_towtrack_response"," " +response);
                    JSONObject obj = new JSONObject(response);
                    obj.getString("request");
                    if (obj.optString("status").equals("true")) {
                        check_model = new ArrayList<>();
                        JSONArray dataArray = obj.getJSONArray("result");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject data_obj = dataArray.getJSONObject(i);
                            playerModel.setAddress(data_obj.getString("towtrack_id"));
                            playerModel.setName(data_obj.getString("towtrack_name"));
                            playerModel.setEmail(data_obj.getString("email"));
                            playerModel.setContact_no(data_obj.getString("contact_no"));
                            playerModel.setCountry_id(data_obj.getString("country_id"));
//                            playerModel.setState(data_obj.getString("state"));
                            playerModel.setCity(data_obj.getString("city_id"));
                            playerModel.setAddress(data_obj.getString("address"));
//                            playerModel.setPinCode(data_obj.getString("pincode"));
                            playerModel.setStatus(data_obj.getString("status"));
                            playerModel.setUpdated_at(data_obj.getString("updated_at"));
                            playerModel.setUser_image(data_obj.getString("towtrack_profile_img"));

                            check_model.add(playerModel);
                        }
                        editor = sharedpreferences.edit();
                        editor.putString(getString(R.string.pref_twotrack_image),playerModel.getUser_image());
//                        editor.putString(getString(R.string.pref_provider_role),playerModel.getProvide_role());
                        editor.apply();

                        if (obj.getString("request").equals("profile")) {
                            if (progressDialog != null)
                                progressDialog.dismiss();
                            Intent intent = new Intent(Act_Login.this, Act_Edit_Profile_Detail.class);
                            intent.putExtra(getString(R.string.pref_towtrack_id), login_model.getUser_id());
                            intent.putExtra(getString(R.string.pref_towtrack_name), login_model.getName());
                            intent.putExtra(getString(R.string.pref_email), login_model.getEmail());
                            intent.putExtra(getString(R.string.pref_country_id), login_model.getCountry_id());
                            intent.putExtra(getString(R.string.pref_contact_no), login_model.getContact_no());
                            intent.putExtra("SPLASH",true);
                            startActivity(intent);
                            finish();
                        }  else  if (obj.getString("request").equals("dashboard")) {
                            if (progressDialog != null)
                                progressDialog.dismiss();
                            Intent intent = new Intent(Act_Login.this, Navigation_Activity.class);
                            startActivity(intent);
                            finish();
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    if (progressDialog != null)
                        progressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (progressDialog != null)
                    progressDialog.dismiss();
//                Toast.makeText(Act_Login.this, "my error :" + error, Toast.LENGTH_LONG).show();
                Log.i("My error", "" + error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> map = new HashMap<>();
                map.put("towtrack_id", id);

                return map;
            }
        };
        queue.add(request);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        if (intent != null && intent.getData() != null) {
            String s0 = intent.getData().toString();
            String s1 = s0.substring(0, 54);
            Log.e("first"," " + s1);
            String s2 = s0.substring(54, s0.length());
            Log.e("second", s2);

            String[] separated = s0.split("/");
            String str1 = separated[0];
            String str2 = separated[1];
            String str3 = separated[2]; // = stackoverflow.com
            String str4 = separated[3]; // = questions
            String str5 = separated[4];
            String str6 = separated[5];
            String str7 = separated[6];
            String str8 = separated[7];

            /*byte[] decodeValue = Base64.decode(s2.trim(), Base64.DEFAULT);
            Log.e("decode", "" + new String(decodeValue));

            String decode = new String();
            decode = new String(decodeValue);*/

            active(str8,str7);
        }
    }

}
