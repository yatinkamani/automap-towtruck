package com.kcs.automaptwotrack;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.kcs.automaptwotrack.Constant.Constant;
import com.kcs.automaptwotrack.Model.Request_Model;
import com.kcs.automaptwotrack.utils.AppUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import me.leolin.shortcutbadger.ShortcutBadger;

public class Act_Tow_Track_Request extends AppCompatActivity implements View.OnClickListener {

    SharedPreferences preferences;

    ImageView img_back;
    TextView txt_title;
    RecyclerView rc_tow_request;
    View lin_error;
    TextView txt_error;
    Button btn_retry;
    AdapterRequest adapterRequest;

    List<Request_Model> request_models = new ArrayList<>();

    String towTrack_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__tow__track__request);

        img_back = findViewById(R.id.img_back);
        txt_title = findViewById(R.id.txt_title);
        rc_tow_request = findViewById(R.id.rc_tow_request);
        lin_error = findViewById(R.id.lin_error);
        txt_error = findViewById(R.id.txtError);
        btn_retry = findViewById(R.id.btnRetry);

        preferences = getSharedPreferences(Act_Login.LOGIN_PREF, MODE_PRIVATE);
        towTrack_id = preferences.getString(getString(R.string.pref_towtrack_id), "");

        btn_retry.setOnClickListener(this);
        img_back.setOnClickListener(this);
        getTowTrackUserRequest();
        InitializeBroadcast();
    }

    // get all towtruck rrequest and set list view
    private void getTowTrackUserRequest() {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.loading));
        dialog.show();
        if (AppUtils.isNetworkAvailable(getApplicationContext())) {
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL + "towtrack_request_list",
                    new Response.Listener<String>() {
                        @SuppressLint({"SetTextI18n", "LongLogTag"})
                        @Override
                        public void onResponse(String response) {
                            Log.e("tow_track_appointment_list", " " + response);
                            dialog.dismiss();
                            request_models = new ArrayList<>();
                            try {
                                Log.e("tow_track_appointment_list", " " + response);
                                JSONObject mainJsonObject = new JSONObject(response);
                                JSONArray result = mainJsonObject.getJSONArray("result");
                                if (result.length() > 0) {

                                    List<Request_Model> s0 = new ArrayList<>();
                                    List<Request_Model> s1 = new ArrayList<>();
                                    List<Request_Model> s2 = new ArrayList<>();
                                    List<Request_Model> s3 = new ArrayList<>();
                                    List<Request_Model> s4 = new ArrayList<>();
                                    List<Request_Model> s5 = new ArrayList<>();
                                    List<Request_Model> s6 = new ArrayList<>();
                                    List<Request_Model> s7 = new ArrayList<>();
                                    List<Request_Model> s8 = new ArrayList<>();

                                    request_models = new ArrayList<>();
                                    for (int i = 0; i < result.length(); i++) {

                                        JSONObject jsonObject = result.getJSONObject(i);
                                        String tow_track_request_id = jsonObject.getString("towtrack_request_id");
                                        String user_id = jsonObject.getString("user_id");
                                        String towtrack_id = jsonObject.getString("towtrack_id");
                                        String user_name = jsonObject.optString("name", "null");
                                        String status = jsonObject.optString("status", "0");
                                        String email = jsonObject.optString("email", "null");
//                                        String city = jsonObject.getString("city");
                                        String user_profile_image = jsonObject.optString("user_image", "");
                                        String contact_no = jsonObject.optString("contact_no", "");
                                        String final_lat = jsonObject.getString("towtrack_latitude");
                                        String final_long = jsonObject.getString("towtrack_longitude");
                                        String lat = jsonObject.getString("user_latitude");
                                        String log = jsonObject.getString("user_longitude");
                                        String user_location = jsonObject.getString("user_location");
                                        String final_destination = jsonObject.optString("final_destination", "");
                                        String car_brand = jsonObject.optString("brand_name", "");
                                        String car_model = jsonObject.optString("model_name", "");
                                        String car_model_year = jsonObject.optString("model_year", "");
                                        String car_fule = jsonObject.optString("fule_type", "");
                                        String create_at = jsonObject.optString("created_at", "");

                                        String timeout_status = "0";
                                        if (jsonObject.has("timeout_status")) {
                                            timeout_status = jsonObject.getString("timeout_status");
                                        }

                                        String towtrack_request_time = jsonObject.getString("towtrack_request_time");
                                        SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
                                        dateFormatGmt.setTimeZone(TimeZone.getTimeZone("GMT"));
                                        Log.e("Tag Timezone Local", dateFormatGmt.format(new Date()));
                                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
                                        Date request_time = null;
                                        Date currentDate = null;
                                        long days = 0;
                                        try {

                                            request_time = format.parse("" + create_at);
                                            currentDate = format.parse(dateFormatGmt.format(new Date()));
                                            Log.e("TAG TIME", "" + format.format(currentDate));
                                            Log.e("TAG TIME", "" + format.format(request_time));

                                            if (currentDate.before(request_time)) {
                                                long diff = request_time.getTime() - currentDate.getTime();
                                                Log.e("Tag days", "" + diff);
                                            } else {
                                                long diff = currentDate.getTime() - request_time.getTime();
                                                Log.e("days", "" + diff);
                                                if (diff > 0) {
                                                    long second = diff / 1000;
                                                    long minute = second / 60;
                                                    long hour = minute / 60;
                                                    days = (hour / 24) + 1;
                                                    Log.e("Tag hour", "" + hour);
                                                    Log.e("Tag day", "" + days);
                                                    if (towtrack_request_time.matches(".*minute*.") ||
                                                            towtrack_request_time.contains("minute")||
                                                            towtrack_request_time.matches(".*minutes*.") ||
                                                            towtrack_request_time.contains("minutes")){
//                                                          Integer.parseInt(mobile_request_time.replaceAll("\\D",""));
                                                        if (minute >= Integer.parseInt(towtrack_request_time.replaceAll("\\D",""))) {
                                                            timeout_status = "1";
                                                        } else {
                                                            timeout_status = "0";
                                                        }
                                                    } else {
                                                        if (hour >= Integer.parseInt(towtrack_request_time.replaceAll("\\D",""))) {
                                                            timeout_status = "1";
                                                        } else {
                                                            timeout_status = "0";
                                                        }
                                                    }
                                                }
                                            }

                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                            Log.e("TAG TIME", e.toString());
                                        }

                                        Request_Model m = new Request_Model(tow_track_request_id, user_id, towtrack_id, user_name, status,
                                                email, "", "", "", user_profile_image, contact_no, "", final_lat, final_long, lat, log,
                                                user_location, final_destination, car_brand, car_model, car_model_year, "", car_fule, timeout_status);

                                        // status 0 or expire time not add list
                                        if ((days > 1 && status.equals("0")) || status.equals("2") || (days > 1 && status.equals("5"))) {

                                        } else {

                                            // status wise change position
                                            if (status.equals("0") && timeout_status.equals("0")) {
                                                s0.add(m);
                                            } else if (status.equals("0") && timeout_status.equals("1")) {
                                                s2.add(m);
                                            } else if (status.equals("1")) {
                                                s1.add(m);
                                            } else if (status.equals("2")) {
                                                s2.add(m);
                                            } else if (status.equals("3") || status.equals("4")) {
                                                s3.add(m);
                                            } else if (status.equals("6")) {
                                                s6.add(m);
                                            } else if (status.equals("5")) {
                                                s5.add(m);
                                            } else if (status.equals("7")) {
                                                s7.add(m);
                                            } else if (status.equals("8")) {
                                                s8.add(m);
                                            } else if (status.equals("9")) {
                                                s5.add(m);
                                            } else if (status.equals("10")) {
                                                s5.add(m);
                                            }
                                        }
                                        /*request_models.add(new Request_Model(tow_track_request_id,user_id,towtrack_id,user_name,status,
                                        email,city,address,pincode,user_profile_image,contact_no,state,final_lat,final_long,lat,log,
                                                user_location,final_destination,car_brand,car_model,car_model_year,car_variant, car_fule, timeout_status));*/
                                    }

                                    request_models = new ArrayList<>();
                                    request_models.addAll(s0);
                                    request_models.addAll(s1);
                                    request_models.addAll(s3);
                                    request_models.addAll(s4);
                                    request_models.addAll(s7);
                                    request_models.addAll(s8);
                                    request_models.addAll(s6);
                                    request_models.addAll(s5);
                                    request_models.addAll(s2);
                                    if (request_models != null && request_models.size() > 0) {

                                        adapterRequest = new AdapterRequest(Act_Tow_Track_Request.this, request_models);
                                        rc_tow_request.setLayoutManager(new GridLayoutManager(Act_Tow_Track_Request.this, 1));
                                        rc_tow_request.setAdapter(adapterRequest);
                                        lin_error.setVisibility(View.GONE);
                                        rc_tow_request.setVisibility(View.VISIBLE);
                                        SharedPreferences preferences = getSharedPreferences(getString(R.string.pref_Badge), MODE_PRIVATE);
                                        SharedPreferences.Editor editor = preferences.edit();
                                        editor.putInt(getString(R.string.pref_badge_count), 0);
                                        editor.apply();
                                        int tot = (preferences.getInt(getString(R.string.pref_badge_count), 0) +
                                                preferences.getInt(getString(R.string.pref_admin_msg_badge_count),0));
                                        if (tot == 0) {
                                            ShortcutBadger.removeCount(getApplicationContext());
                                        } else {
                                            ShortcutBadger.applyCount(getApplicationContext(), tot);
                                        }
                                        Log.e("TAG", "onResponse: " + request_models.size());
                                        Log.e("TAG", "onResponse: " + adapterRequest.getItemCount());
                                    } else {

                                        lin_error.setVisibility(View.VISIBLE);
                                        rc_tow_request.setVisibility(View.GONE);
                                        txt_error.setText("No Data Found");
                                        btn_retry.setVisibility(View.GONE);

                                    }
                                } else {
                                    lin_error.setVisibility(View.VISIBLE);
                                    txt_error.setText("No Data Found");
                                    btn_retry.setVisibility(View.GONE);
                                    rc_tow_request.setVisibility(View.GONE);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                Log.e("size == > Error", e.toString());
                                lin_error.setVisibility(View.VISIBLE);
//                                txt_error.setText(getString(R.string.something_wrong_please));
                                txt_error.setText(getString(R.string.no_data_fond));
                                btn_retry.setVisibility(View.GONE);
                                rc_tow_request.setVisibility(View.GONE);
                                dialog.dismiss();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("size ==> Error", error.toString());
                            lin_error.setVisibility(View.VISIBLE);
//                            txt_error.setText(getString(R.string.something_wrong_please));
                            txt_error.setText(getString(R.string.no_data_fond));
                            btn_retry.setVisibility(View.VISIBLE);
                            rc_tow_request.setVisibility(View.GONE);
                            dialog.dismiss();
                        }
                    }) {

                @Override
                public Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("towtrack_id", towTrack_id);
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(3000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        } else {
            lin_error.setVisibility(View.VISIBLE);
            txt_error.setText(getString(R.string.internet_not_connect));
            btn_retry.setVisibility(View.VISIBLE);
            rc_tow_request.setVisibility(View.GONE);
            dialog.dismiss();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == btn_retry) {
            getTowTrackUserRequest();
        } else if (view == img_back){
            onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getTowTrackUserRequest();
    }

    static class AdapterRequest extends RecyclerView.Adapter<AdapterRequest.ViewHolder> {

        Context context;
        List<Request_Model> request_models;

        public AdapterRequest(Context context, List<Request_Model> request_models) {
            this.context = context;
            this.request_models = request_models;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(context).inflate(R.layout.adapter_request, viewGroup, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final ViewHolder holder, int i) {
            final Request_Model request_model = request_models.get(i);

            /*Glide.with(context).asBitmap().load(request_model.getUser_profile_image()).placeholder(R.drawable.user).thumbnail(0.01f)
                    .into(new BitmapImageViewTarget(holder.img_user_image){
                        @Override
                        protected void setResource(Bitmap resource) {
                            holder.img_user_image.setImageBitmap(resource);
                        }
                    });*/

            holder.txt_user_name.setText(AppUtils.getCapsSentences(request_model.getUser_name()));
            holder.txt_model.setText(Html.fromHtml(request_model.getCar_model() + " " + request_model.getCar_model_year()));
            holder.txt_user_location.setText(Html.fromHtml(request_model.getUser_location()));

            if (request_model.getCity().equals("")){
                holder.lin_city.setVisibility(View.GONE);
            } else {
                holder.txt_city.setText(Html.fromHtml(request_model.getCity()));
                holder.lin_city.setVisibility(View.VISIBLE);
            }

            if (request_model.getFinal_destination().equals("")){
                holder.lin_final_destinatio.setVisibility(View.GONE);
            } else {
                holder.txt_final_destination.setText(Html.fromHtml(request_model.getFinal_destination()));
                holder.lin_final_destinatio.setVisibility(View.VISIBLE);
            }

            holder.main_card.setEnabled(true);
            // new request
            if (request_model.getStatus().equalsIgnoreCase("0")) {
                holder.main_card.setCardBackgroundColor(context.getResources().getColor(R.color.red_low_opacity));
                holder.txt_status.setText(R.string.neww);
                // expire request time out
                if (request_model.getTime_out_status().equals("1")) {
                    holder.txt_status.setText(R.string.time_out_for_participate);
                    holder.main_card.setEnabled(false);
                }
            }
            // towtruck owner offered request
            else if (request_model.getStatus().equalsIgnoreCase("1")) {
                holder.main_card.setCardBackgroundColor(context.getResources().getColor(R.color.yellow_low_opacity));
                holder.txt_status.setText(R.string.offers_request);
            }
            // towtruck owner Ignored request
            else if (request_model.getStatus().equalsIgnoreCase("2")) {
                holder.main_card.setCardBackgroundColor(context.getResources().getColor(R.color.light_gray));
                holder.txt_status.setText(R.string.ignor);
            }
            // customer chose towtruck offer request
            else if (request_model.getStatus().equalsIgnoreCase("3")) {
                holder.main_card.setCardBackgroundColor(context.getResources().getColor(R.color.green_low_opacity));
                holder.txt_status.setText(R.string.congratulation_you_have_chosen);
            }
            // customer chose towtruck offer request
            else if (request_model.getStatus().equalsIgnoreCase("4")) {
                holder.main_card.setCardBackgroundColor(context.getResources().getColor(R.color.green_low_opacity));
                holder.txt_status.setText(R.string.congratulation_you_have_chosen);
            }
            // customer chose other towtruck offer request
            else if (request_model.getStatus().equalsIgnoreCase("6")) {
                holder.main_card.setCardBackgroundColor(context.getResources().getColor(R.color.light_gray));
                holder.txt_status.setText(R.string.sorry_customer_chose_another);
            }
            // customer cancel offer request
            else if (request_model.getStatus().equalsIgnoreCase("5")) {
                holder.main_card.setCardBackgroundColor(context.getResources().getColor(R.color.light_gray));
                holder.txt_status.setText(R.string.cancel_or_order_request);
            }
            // customer rate this offer request
            else if (request_model.getStatus().equalsIgnoreCase("7")) {
                holder.main_card.setCardBackgroundColor(context.getResources().getColor(R.color.quantum_yellowA700));
                holder.txt_status.setText(R.string.please_rate_service);
            }
            // complete this offer request
            else if (request_model.getStatus().equalsIgnoreCase("8")) {
                holder.main_card.setCardBackgroundColor(context.getResources().getColor(R.color.quantum_vanillagreenA400));
                holder.txt_status.setText(R.string.complete_your_service);
            }
            // customer complain towtruck offer request
            else if (request_model.getStatus().equalsIgnoreCase("9")) {
                holder.main_card.setCardBackgroundColor(context.getResources().getColor(R.color.quantum_vanillagreenA400));
                holder.txt_status.setText(R.string.the_user_has_complain_to_you);
            }
            // towtruck owner complain customer request
            else if (request_model.getStatus().equalsIgnoreCase("10")) {
                holder.main_card.setCardBackgroundColor(context.getResources().getColor(R.color.quantum_vanillagreenA400));
                holder.txt_status.setText(R.string.you_complain_to_user);
            }

            holder.main_card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (request_model.getStatus().equals("2")) {
                        Toast.makeText(context, context.getString(R.string.you_have_ignore_this_request), Toast.LENGTH_LONG).show();
                    } else if (request_model.getStatus().equals("8")) {
                        Toast.makeText(context, context.getString(R.string.complete_tow_truck_service), Toast.LENGTH_LONG).show();
                    } else if (request_model.getStatus().equals("9") || request_model.getStatus().equals("10") ) {
                        Toast.makeText(context, context.getString(R.string.complained_request), Toast.LENGTH_LONG).show();
                    } else if (request_model.getStatus().equals("6")) {
                        Toast.makeText(context, context.getString(R.string.sorry_customer_choose_else), Toast.LENGTH_LONG).show();
                    } else {

                        Intent intent = new Intent(context, Act_TowTrack_Request_Detail.class);
                        intent.putExtra(context.getString(R.string.pref_request_id), request_model.getTow_track_request_id());
                        context.startActivity(intent);
                    }
                }
            });
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemCount() {
            return request_models.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public ImageView img_user_image;
            TextView txt_user_name, txt_city, txt_model, txt_user_location, txt_final_destination, txt_status;
            LinearLayout lin_city, lin_model, lin_user_location, lin_final_destinatio, lin_status;
            CardView main_card;

            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                img_user_image = itemView.findViewById(R.id.img_user_image);
                txt_user_name = itemView.findViewById(R.id.txt_user_name);
                txt_city = itemView.findViewById(R.id.txt_city);
                lin_city = itemView.findViewById(R.id.lin_city);
                txt_model = itemView.findViewById(R.id.txt_model);
                lin_model = itemView.findViewById(R.id.lin_model);
                txt_user_location = itemView.findViewById(R.id.txt_user_location);
                lin_user_location = itemView.findViewById(R.id.lin_user_location);
                txt_final_destination = itemView.findViewById(R.id.txt_final_destination);
                lin_final_destinatio = itemView.findViewById(R.id.lin_final_destination);
                main_card = itemView.findViewById(R.id.main_card);
                lin_status = itemView.findViewById(R.id.lin_status);
                txt_status = itemView.findViewById(R.id.txt_status);
            }
        }
    }

    public void InitializeBroadcast() {
        LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(Act_Tow_Track_Request.this);
        lbm.registerReceiver(receiver, new IntentFilter("send_data"));
    }

    // new request to referss api
    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int user = intent.getIntExtra(getString(R.string.pref_badge_count), 0);

            if (user != 0 && !Act_Tow_Track_Request.this.isFinishing()){
                getTowTrackUserRequest();
            }
        }
    };

}
