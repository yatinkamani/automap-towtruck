package com.kcs.automaptwotrack.Constant;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class Constant {

   public static  int i;
   public static  int i2;
   public static String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
   public static String OUT_JSON = "/json";
   public static String API_KEY = "AIzaSyBBIiuqET41PjtO4SvCiA6T4BYJzS3OSy0";

   public static String TYPE_AUTOCOMPLETE = "/autocomplete";

   public static final String LOGIN_PREF = "Login_preference" ;
//   public static final String URL = "http://mobwebdemo.xyz/automap/api/" ;
//   public static final String URL = "http://arccus.in/webmobdemo/automap/api/" ;
   public static final String URL = "http://kaprat.com/dev/automap/api/" ;
   public static final String SERVICE_IMAGE_URL = "http://kaprat.com/automap/assets/uploads/Service_Img/" ;

   public static String getFormattedDateWithTime(String simple_date) {
      try {
         SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd k:mm:ss");
         Date date = fmt.parse(simple_date);

         SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM,yyyy");
         return fmtOut.format(date);
      } catch (Exception e) {
         e.printStackTrace();
         return simple_date;
      }
   }

   public static List<String> getStringToArray(String Value){
      List<String> strings = new ArrayList<>();

      if (Value != null){
         String s[] = Value.split(",");
         strings.addAll(Arrays.asList(s));
      }
      return strings;
   }

}
