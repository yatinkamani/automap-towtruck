package com.kcs.automaptwotrack;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;

import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.android.material.navigation.NavigationView;

import androidx.appcompat.app.AlertDialog;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.kcs.automaptwotrack.BackGroundNotification.LocationUpdateService;
import com.kcs.automaptwotrack.Constant.Constant;
import com.kcs.automaptwotrack.Model.MenuModel;
import com.kcs.automaptwotrack.Model.Request_Model;
import com.kcs.automaptwotrack.adapter.ExpandableListAdapter;
import com.kcs.automaptwotrack.utils.API;
import com.kcs.automaptwotrack.utils.APIResponse;
import com.kcs.automaptwotrack.utils.AppUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;
import me.leolin.shortcutbadger.ShortcutBadger;

import static com.kcs.automaptwotrack.Act_Login.LOGIN_PREF;

public class Navigation_Activity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    ExpandableListAdapter expandableListAdapter;
    ExpandableListView expandableListView;
    List<MenuModel> headerList = new ArrayList<>();
    HashMap<MenuModel, List<MenuModel>> childList = new HashMap<>();
    //    View linError;
    //    TextView txtError;
    //    Button btnRetry;

    SharedPreferences prefs;
    String user_email, user_name, user_image;
    View header;
    TextView txt_user_email, txt_user_name;
    CircleImageView imageView;

    RelativeLayout rel_request, rel_admin_msg;
    String towtrack_id = "", provider_role = "";
    //    List<Request_Model> request_models = new ArrayList<>();

    TextView txt_msg_count, txt_request_count;
    BroadcastReceiver receiver;
    //    private Act_Tow_Track_Request.AdapterRequest adapterRequest;
    //    private RecyclerView rc_tow_request;
    Location location;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
//        applyOverrideConfiguration(new Configuration());
    }

    @Override
    public void applyOverrideConfiguration(Configuration overrideConfiguration) {
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT <= 25) {
            //Use you logic to update overrideConfiguration locale
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            String lng = preferences.getString(getString(R.string.pref_language), "en");
            Log.e("Tag LANG+++\n", lng);
            Locale locale = new Locale(lng);//your own implementation here
            overrideConfiguration.setLocale(locale);
        }
        super.applyOverrideConfiguration(overrideConfiguration);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefs = getSharedPreferences(LOGIN_PREF, Context.MODE_PRIVATE);
        String language = prefs.getString(getString(R.string.pref_language), "en").trim();
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        if (Locale.getDefault().getLanguage().equals("ar"))
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        else
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        setContentView(R.layout.activity_navigation);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        expandableListView = findViewById(R.id.expandableListView);
        //        rc_tow_request = findViewById(R.id.rc_tow_request);
        //        btnRetry = findViewById(R.id.btnRetry);
        //        btnRetry.setVisibility(View.GONE);
        //        linError = findViewById(R.id.lin_error);
        //        txtError = findViewById(R.id.txtError);

        txt_msg_count = findViewById(R.id.txt_msg_count);
        txt_request_count = findViewById(R.id.txt_request_count);
        rel_admin_msg = findViewById(R.id.rel_admin_msg);
        rel_request = findViewById(R.id.rel_request_count);

        user_email = prefs.getString(getString(R.string.pref_email), "");
        user_name = prefs.getString(getString(R.string.pref_towtrack_name), "");
        user_image = prefs.getString(getString(R.string.pref_twotrack_image), "");
        towtrack_id = prefs.getString(getString(R.string.pref_towtrack_id), "");
        //        service_type  = prefs.getString(getString(R.string.pref_special_service_name),"");
//        Log.e("Pref","user email"+user_email+" service Name "+service_type);

        provider_role = prefs.getString(getString(R.string.pref_provider_role), "");

        UpdateView(user_email,user_image,user_name);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);

        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        header = navigationView.getHeaderView(0);

        startService(new Intent(getApplicationContext(), LocationUpdateService.class));
        prepareMenuData();
        populateExpandableList();
        UpdateLocation();
        //        towtrack_request.setText(getString(R.string.user_request));

        /*btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getTowTrackUserRequest();
            }
        });*/

        /*towtrack_request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Act_Tow_Track_Request.class);
                startActivity(intent);
            }
        });*/

        rel_request.setOnClickListener(this);
        rel_admin_msg.setOnClickListener(this);

        // notification badge count in preference
        SharedPreferences preferences = getSharedPreferences(getString(R.string.pref_Badge), MODE_PRIVATE);
        onBadgeCount(preferences.getInt(getString(R.string.pref_badge_count), 0), preferences.getInt(getString(R.string.pref_admin_msg_badge_count), 0));
        Log.e("Tag Badge count", "" + preferences.getInt(getString(R.string.pref_badge_count), 0));
        LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(this);
        lbm.registerReceiver(receiver, new IntentFilter("send_data"));

    }

    Handler handler = null;
    Runnable runnable = null;

    Location lastLocation = null;

    // update towtruck location
    private void UpdateLocation() {

        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                runnable = this;
                location = LocationUpdateService.locations;

                if (location != null && (location != lastLocation)) {
                    lastLocation = location;
                    if (AppUtils.isNetworkAvailable(getApplicationContext())) {
                        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL + "towtrack_location_update",
                                new Response.Listener<String>() {
                                    @SuppressLint("SetTextI18n")
                                    @Override
                                    public void onResponse(String response) {
                                        Log.e("Tag response", response);
                                        response = Html.fromHtml(response).toString();
                                        response = response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1);
                                        try {
                                            JSONObject mainJsonObject = new JSONObject(response);
                                            if (mainJsonObject.getString("status").equals("1")) {

                                                Log.e("Tag response", mainJsonObject.toString());
                                            }

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Log.e("size ==> Error", error.toString());
                                    }
                                }) {

                            @Override
                            public Map<String, String> getParams() {
                                Map<String, String> params = new HashMap<>();
                                params.put("towtrack_id", towtrack_id);
                                params.put("latitude", String.valueOf(location.getLatitude()));
                                params.put("longitude", String.valueOf(location.getLongitude()));
                                Log.e("Tag", params.toString());
                                return params;
                            }
                        };
                        stringRequest.setRetryPolicy(new DefaultRetryPolicy(3000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                        RequestQueue requestQueue = Volley.newRequestQueue(Navigation_Activity.this);
                        requestQueue.add(stringRequest);
                    } else {
//                         handler.postDelayed(this,5000);
                        Toast.makeText(getApplicationContext(), getString(R.string.internet_not_connect), Toast.LENGTH_LONG).show();
                        Log.e("Tag Location", "no internet");
                    }
                } else {
                    Log.e("Tag Location", "no location");
                }
                handler.postDelayed(this, 30000);
            }
        }, 3000);
    }

    // drawer sidebar menu item add
    private void prepareMenuData() {

        /*MenuModel menuModel = new MenuModel(R.drawable.ic_notifications_none_black_24dp, getString(R.string.user_request), true, false, R.drawable.trans); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);*/

        MenuModel menuModel = new MenuModel(R.drawable.users, getString(R.string.my_profile), true, false, R.drawable.trans); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);

        menuModel = new MenuModel(R.drawable.langage, getString(R.string.change_language), true, false, R.drawable.trans); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);

        menuModel = new MenuModel(android.R.drawable.ic_menu_edit, getString(R.string.change_password), true, false, R.drawable.trans); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);

        menuModel = new MenuModel(android.R.drawable.ic_menu_share, getString(R.string.share), true, false, R.drawable.trans); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);

        menuModel = new MenuModel(android.R.drawable.ic_dialog_info, getString(R.string.about_us), true, true, 0); //Menu of Java Tutorials
        headerList.add(menuModel);

        menuModel = new MenuModel(R.drawable.contact_us, getString(R.string.Contact_us), true, true, 0); //Menu of Java Tutorials
        headerList.add(menuModel);

        menuModel = new MenuModel(R.drawable.domain, getString(R.string.website_link), true, true, 0); //Menu of Java Tutorials
        headerList.add(menuModel);

        menuModel = new MenuModel(R.drawable.icon_logout, getString(R.string.Sign_Out), true, false, R.drawable.trans); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }
    }

    // drawer sidebar menu item click event
    private void populateExpandableList() {

        expandableListAdapter = new ExpandableListAdapter(this, headerList, childList);
        expandableListView.setAdapter(expandableListAdapter);

        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

                if (headerList.get(groupPosition).isGroup) {
                    if (headerList.get(groupPosition).menuName == getString(R.string.Sign_Out)) {
                        SharedPreferences settings = getApplicationContext().getSharedPreferences(LOGIN_PREF, Context.MODE_PRIVATE);
                        settings.edit().clear().apply();
                        Intent intent = new Intent(Navigation_Activity.this, Act_Login.class);
                        startActivity(intent);
                        finish();
                    } else if (headerList.get(groupPosition).menuName == getString(R.string.change_password)) {
                        Intent intent = new Intent(Navigation_Activity.this, Act_change_password.class);
                        startActivity(intent);
                    } /*else if (headerList.get(groupPosition).menuName == getString(R.string.user_request)) {
                        Intent intent = new Intent(Navigation_Activity.this, Act_Tow_Track_Request.class);
                        startActivity(intent);
                    } */else if (headerList.get(groupPosition).menuName == getString(R.string.change_language)) {
                        SelectLanguageDialog();
                    } else if (headerList.get(groupPosition).menuName == getString(R.string.about_us)) {
                        Intent intent = new Intent(Navigation_Activity.this, Act_About_Us.class);
                        startActivity(intent);
                    } else if (headerList.get(groupPosition).menuName == getString(R.string.Contact_us)) {
                        Intent intent = new Intent(Navigation_Activity.this, Act_Contact_Us.class);
                        startActivity(intent);
                    } else if (headerList.get(groupPosition).menuName == getString(R.string.my_profile)) {
                        Constant.i = 1;
                        Intent intent = new Intent(Navigation_Activity.this, Act_Edit_Profile_Detail.class);
                        startActivity(intent);
                    } else if (headerList.get(groupPosition).menuName == getString(R.string.website_link)) {
                        OpenWebsite();
                    } else if (headerList.get(groupPosition).menuName == getString(R.string.share)) {
                        shareApp();
                    }/*else if (headerList.get(groupPosition).menuName == getString(R.string.Contact_us)) {
                        Constant.i = 1;
                        Intent intent = new Intent(Navigation_Activity.this, Act_Contact_Us.class);
                        startActivity(intent);
                    }*/
                    if (!headerList.get(groupPosition).hasChildren) {
                        onBackPressed();
                    }
                }

                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                }
                return false;
            }
        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                if (childList.get(headerList.get(groupPosition)) != null) {
                    MenuModel model = childList.get(headerList.get(groupPosition)).get(childPosition);
                }
                return false;
            }
        });
    }

    public void SelectLanguageDialog() {

        final String[] language = {getString(R.string.lang_english), getString(R.string.lang_arabic)};
        prefs = getSharedPreferences(LOGIN_PREF, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = prefs.edit();
        String selected_lang = prefs.getString(getString(R.string.pref_language), "en");
        int checked = -1;
        if (selected_lang.toString().equals("en")) {
            checked = 0;
        } else if (selected_lang.toString().equals("ar")) {
            checked = 1;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(Navigation_Activity.this);
        builder.setTitle("Select Language");
        builder.setSingleChoiceItems(language, checked, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (language[i].equals(getString(R.string.lang_english))) {
//                    editor.putString(getString(R.string.pref_language), "en").apply();
                    changeLangApi("en");
                } else {
                    changeLangApi("ar");
//                    editor.putString(getString(R.string.pref_language), "ar").apply();
                }
                dialogInterface.dismiss();
                recreate();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    // change language update view
    private void setLangRecreate(String LocalName){
        prefs = getSharedPreferences(LOGIN_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(getString(R.string.pref_language), LocalName).apply();
        startActivity(new Intent(getApplicationContext(), Navigation_Activity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    // change language in api
    private void changeLangApi(String localeName){
        API api = new API(Navigation_Activity.this, new APIResponse() {
            @Override
            public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                if (isSuccess){
                    Log.e("Tag response", response);
                    setLangRecreate(localeName);
                }
            }

            @Override
            public void onAPIError(int requestCode, boolean isError, String error) {
                Toast.makeText(getApplicationContext(),getString(R.string.something_wrong_please), Toast.LENGTH_LONG).show();
            }
        });

        Map<String, String> map = new HashMap<>();
        String language = "";
        if (localeName.equals("ar")){
            language = "Arabic";
        }else {
            language = "English";
        }
        map.put("language",language);
        map.put("towtrack_id", towtrack_id);
        api.execute(100, Constant.URL+"changeLanguage", map, false);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (handler != null && runnable != null) {
                handler.removeCallbacks(runnable);
            }
            stopService(new Intent(getApplicationContext(), LocationUpdateService.class));
            Intent a = new Intent(Intent.ACTION_MAIN);
            a.addCategory(Intent.CATEGORY_HOME);
            a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(a);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_service_book) {
            // Handle the camera action
        } else if (id == R.id.nav_car) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_my_profile) {
            Constant.i = 1;
            Intent intent = new Intent(Navigation_Activity.this, Act_Edit_Profile_Detail.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.navigation_, menu);
        return true;
    }

    // badge count broadcast receiver
    public void onBadgeCount(int request_count, int msg_count) {

        Log.e("Tag count", "" + request_count);
        final int[] int_count = {0, 1};
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent != null) {
                    int_count[0] = intent.getIntExtra(getString(R.string.pref_badge_count), 0);
                    Log.e("Tag count Intent", "" + int_count[0]);
                    if (txt_request_count != null) {
                        Log.e("Tag count Text", "" + txt_request_count.getText());
                        if (int_count[0] == 0) {
                            txt_request_count.setVisibility(View.GONE);
                        } else {
                            txt_request_count.setVisibility(View.VISIBLE);
                            txt_request_count.setText("" + int_count[0]);
                        }
                    } else {
                        Log.e("Tag count Text ", "NULL NULL NULL");
                    }

                    int_count[1] = intent.getIntExtra(getString(R.string.pref_admin_msg_badge_count), 0);
                    Log.e("Tag count Intent", "" + int_count[1]);
                    if (txt_msg_count != null) {
                        Log.e("Tag count Text", "" + txt_msg_count.getText());
                        if (int_count[1] == 0) {
                            txt_msg_count.setVisibility(View.GONE);
                        } else {
                            txt_msg_count.setVisibility(View.VISIBLE);
                            txt_msg_count.setText("" + int_count[1]);
                        }
                    } else {
                        Log.e("Tag count Text ", "NULL NULL NULL");
                    }
                }
            }
        };
        if (txt_request_count != null) {
            if (request_count == 0) {
                txt_request_count.setVisibility(View.GONE);
            } else {
                txt_request_count.setText("" + request_count);
            }
        }

        if (txt_msg_count != null) {
            if (msg_count == 0) {
                txt_msg_count.setVisibility(View.GONE);
            } else {
                txt_msg_count.setText("" + msg_count);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences prefs = getSharedPreferences(Act_Login.LOGIN_PREF,MODE_PRIVATE);
        user_email = prefs.getString(getString(R.string.pref_email), "");
        user_name = prefs.getString(getString(R.string.pref_towtrack_name), "");
        user_image = prefs.getString(getString(R.string.pref_twotrack_image), "");
        UpdateView(user_email,user_image,user_name);
        SharedPreferences preferences = getSharedPreferences(getString(R.string.pref_Badge), MODE_PRIVATE);
        onBadgeCount(preferences.getInt(getString(R.string.pref_badge_count), 0), preferences.getInt(getString(R.string.pref_admin_msg_badge_count), 0));
        Log.e("Tag Badge count", "" + preferences.getInt(getString(R.string.pref_badge_count), 0));
    }

    @Override
    public void onClick(View view) {
        if (view == rel_request) {
            Intent intent = new Intent(getApplicationContext(), Act_Tow_Track_Request.class);
            startActivity(intent);
        } else if (view == rel_admin_msg) {
            Intent intent = new Intent(getApplicationContext(), Act_Admin_Message.class);
            startActivity(intent);
        }
    }

    // update data in sidebase
    public void UpdateView(String user_email, String user_image, String user_name) {

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        header = navigationView.getHeaderView(0);
        txt_user_email = (TextView) header.findViewById(R.id.txt_user_email);
        txt_user_email.setText(user_email);

        txt_user_name = (TextView) header.findViewById(R.id.txt_user_name);
        txt_user_name.setText(user_name);

        imageView = (CircleImageView) header.findViewById(R.id.imageView);

        Log.e("ppp", "onCreate: " + user_image);
        Glide.with(getApplicationContext()).asBitmap().load(user_image).thumbnail(0.01f)
                .apply(new RequestOptions().placeholder(R.drawable.tow_truck_icon))
                .into(new BitmapImageViewTarget(imageView) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        imageView.setImageBitmap(resource);
                    }
                });
    }

    private void OpenWebsite() {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://automap.repair/")));
    }

    private void shareApp() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT,
                "Hey check out this app at: https://play.google.com/store/apps/details?id=" + Navigation_Activity.this.getPackageName());
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }
}
