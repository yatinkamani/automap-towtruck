package com.kcs.automaptwotrack;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.CountDownTimer;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.kcs.automaptwotrack.Constant.Constant;
import com.kcs.automaptwotrack.Model.Contry_spinner_model;
import com.kcs.automaptwotrack.Model.Register_model;
import com.kcs.automaptwotrack.newPackage.Helper;
import com.kcs.automaptwotrack.utils.API;
import com.kcs.automaptwotrack.utils.APIResponse;
import com.kcs.automaptwotrack.utils.AppUtils;
import com.mukesh.OtpView;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.kcs.automaptwotrack.Act_Login.isValidEmail;

public class Act_Registration extends AppCompatActivity implements View.OnClickListener {

    EditText ed_name, ed_password, ed_mobile, ed_confirm_password;
    Spinner spinner_country;
    TextView btn_reg;
    TextView textView, tv_ph_code;
    ImageView login_back;

    String c_id;
    String con_cate;
    String c_currency;
    private ArrayList<Contry_spinner_model> country_spinner_models;
    private ArrayList<String> names = new ArrayList<String>();
    ProgressDialog progress;
    Register_model regModel = new Register_model();
    private ArrayList<Register_model> Reg_Model;
    String st_nm, st_email, st_pass, st_mobile, st_confirm_pass;
    boolean isVerified = false;
    PhoneAuthProvider.OnVerificationStateChangedCallbacks callbacks;
    FirebaseAuth auth;
    PhoneAuthProvider.ForceResendingToken token;
    String verificationId;
    AlertDialog dialog;

    PhoneAuthCredential authCredential;
    boolean isInstant = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__registration);

        ed_name = findViewById(R.id.Ed_name);
//        ed_email = findViewById(R.id.Ed_Email);
        ed_password = findViewById(R.id.Ed_PASSWORD);
        ed_mobile = findViewById(R.id.Ed_contact);
        ed_confirm_password = findViewById(R.id.Ed_confirm_password);
        spinner_country = findViewById(R.id.Sp_select_country);
        textView = findViewById(R.id.tv_teg);
        login_back = findViewById(R.id.login_back);
        tv_ph_code = findViewById(R.id.tv_ph_code);

        auth = FirebaseAuth.getInstance();
        retrieveJSON();
        btn_reg = findViewById(R.id.Btn_registration);
        login_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btn_reg.setOnClickListener(this);

        // country select spinner
        spinner_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                st_mobile = ed_mobile.getText().toString();
                country_spinner_models.get(i).getCon_currency();
                String p_code = country_spinner_models.get(i).getCon_ph_code();
                c_id = country_spinner_models.get(i).getCon_id();
                c_currency = country_spinner_models.get(i).getCon_currency();
                tv_ph_code.setText("+" + p_code);
                con_cate = st_mobile;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // DO Nothing here
            }
        });

        textView.setOnClickListener(this);
    }

    // get country data and set spinner
    private void retrieveJSON() {

        API api = new API(getApplicationContext(), new APIResponse() {
            @Override
            public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                Log.e("Country Response", response);
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.optString("status").equals("true")) {
                        country_spinner_models = new ArrayList<>();
                        JSONObject object = obj.getJSONObject("result");
                        JSONArray dataArray = object.getJSONArray("country");

                        for (int i = 0; i < dataArray.length(); i++) {

                            Contry_spinner_model playerModel = new Contry_spinner_model();
                            JSONObject data_obj = dataArray.getJSONObject(i);
                            playerModel.setCon_id(data_obj.getString("country_id"));
                            playerModel.setCon_code(data_obj.getString("country_code"));
                            playerModel.setCon_name(data_obj.getString("country_name"));
                            playerModel.setCon_currency(data_obj.getString("currency"));
                            playerModel.setCon_language(data_obj.getString("language"));
                            playerModel.setCon_ph_code(data_obj.getString("phone_code"));
                            country_spinner_models.add(playerModel);
                        }
                        for (int i = 0; i < country_spinner_models.size(); i++) {
                            names.add(country_spinner_models.get(i).getCon_name().toString());
                        }
                        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(Act_Registration.this, android.R.layout.simple_spinner_dropdown_item, names);
                        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                        spinner_country.setAdapter(spinnerArrayAdapter);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onAPIError(int requestCode, boolean isError, String error) {
                Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
            }
        });

        api.execute(200,Constant.URL + "get_country",null,false);

        /*StringRequest stringRequest = new StringRequest(Request.Method.GET, Constant.URL + "get_country",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Country Response", response);
                        try {
                            JSONObject obj = new JSONObject(response);
                            if (obj.optString("status").equals("true")) {
                                country_spinner_models = new ArrayList<>();
                                JSONObject object = obj.getJSONObject("result");
                                JSONArray dataArray = object.getJSONArray("country");

                                for (int i = 0; i < dataArray.length(); i++) {

                                    Contry_spinner_model playerModel = new Contry_spinner_model();
                                    JSONObject data_obj = dataArray.getJSONObject(i);
                                    playerModel.setCon_id(data_obj.getString("country_id"));
                                    playerModel.setCon_code(data_obj.getString("country_code"));
                                    playerModel.setCon_name(data_obj.getString("country_name"));
                                    playerModel.setCon_currency(data_obj.getString("currency"));
                                    playerModel.setCon_language(data_obj.getString("language"));
                                    playerModel.setCon_ph_code(data_obj.getString("phone_code"));
                                    country_spinner_models.add(playerModel);
                                }
                                for (int i = 0; i < country_spinner_models.size(); i++) {
                                    names.add(country_spinner_models.get(i).getCon_name().toString());
                                }
                                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(Act_Registration.this, android.R.layout.simple_spinner_dropdown_item, names);
                                spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                                spinner_country.setAdapter(spinnerArrayAdapter);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //  displaying the error in toast if occurs
                Log.e("Tag Error", error.toString());
//                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        // request queue
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(3000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);*/
    }

    public static boolean isValidPhone(String phone) {
        String expression = "^([0-9\\+]|\\(\\d{1,3}\\))[0-9\\-\\. ]{3,15}$";
        CharSequence inputString = phone;
        Pattern pattern = Pattern.compile(expression);
        Matcher matcher = pattern.matcher(inputString);
        if (matcher.matches()) {
            return true;
        } else {
            return false;
        }
    }

    // validation check
    public void Register() {

        st_nm = ed_name.getText().toString();
//        st_email = ed_email.getText().toString();
        st_pass = ed_password.getText().toString();
        st_confirm_pass = ed_confirm_password.getText().toString();
        st_mobile = ed_mobile.getText().toString();

        /*if (TextUtils.isEmpty(st_nm)) {
            ed_name.setError("Enter user name");
        } else if (TextUtils.isEmpty(st_email)) {
            ed_email.setError("Enter email");
        } else if (!isValidEmail(st_email)) {
            ed_email.setError("Email is not valid");
        } else */if (TextUtils.isEmpty(st_pass)) {
            ed_password.setError(getString(R.string.password));
        } else if (st_pass.length() < 6) {
            ed_password.setError(getString(R.string.enter_6digit_password));
        } else if (TextUtils.isEmpty(st_confirm_pass)) {
            ed_confirm_password.setError(getString(R.string.confirm_password));
        } else if (!st_pass.matches(st_confirm_pass)) {
            ed_confirm_password.setError(getString(R.string.does_not_match_password));
        } else if (TextUtils.isEmpty(st_mobile)) {
            ed_mobile.setError(getString(R.string.contact_no));
        } else if (!isValidNumber()) {
            return;
        } else if (st_mobile.length() < 6 || st_mobile.length() > 13) {
            ed_mobile.setError(getString(R.string.enter_valid_contact));
        } else if (!Helper.isNetworkAvailable(getApplicationContext())) {
            Toast.makeText(getApplicationContext(), R.string.internet_not_connect, Toast.LENGTH_LONG).show();
        } else {
            checkNumberEmail();
//                    OtpDialog();
        }
    }

    // check number already register
    ProgressDialog sendOtpDialog;
    private void checkNumberEmail() {

        PhoneCallBack(true);
        API api = new API(Act_Registration.this, new APIResponse() {
            @Override
            public void onAPISuccess(int requestCode, boolean isSuccess, String response) {

                Log.e("Tag Response", response);

                JSONObject object = null;
                try {
                    object = new JSONObject(response);
                    if (object.getString("status").equals("true")) {
                        sendOtpDialog = new ProgressDialog(Act_Registration.this);
                        sendOtpDialog.setMessage(getString(R.string.loading));
                        sendOtpDialog.show();
                        PhoneAuthProvider.getInstance().verifyPhoneNumber(tv_ph_code.getText().toString() + st_mobile
                                , 2
                                , TimeUnit.MINUTES
                                , Act_Registration.this
                                , callbacks);
                    } else {
                        Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), getString(R.string.something_wrong_please), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onAPIError(int requestCode, boolean isError, String error) {
                Toast.makeText(getApplicationContext(), getString(R.string.something_wrong_please), Toast.LENGTH_LONG).show();
            }
        });
        Map<String, String> map = new HashMap<>();
        map.put("contact_no", st_mobile);
//        map.put("email", st_email);

        api.execute(100, Constant.URL + "checkTowTruckEmailAndContact", map, true);
    }

    // register api call
    private void RegisterApi() {
        progress = new ProgressDialog(Act_Registration.this);
        progress.setMessage("Wait a moment until verification link not sent...");
        progress.show();
        StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "towtrack_register", new Response.Listener<String>() {
            @SuppressLint("LongLogTag")
            @Override
            public void onResponse(String response) {
                Log.e("Tow Truck register_response", " " + response);
                progress.dismiss();
                try {

                    JSONObject obj = new JSONObject(response);
                    Reg_Model = new ArrayList<>();
                    if (obj.optString("status").equals("true")) {

//                        Toast.makeText(Act_Registration.this, obj.optString("message"), Toast.LENGTH_SHORT).show();
                        Toast.makeText(Act_Registration.this, getString(R.string.regiter_successfulyy), Toast.LENGTH_SHORT).show();

                        if (dialog != null) {
                            dialog.dismiss();
                            Intent intent = new Intent(getApplicationContext(),Act_Login.class);
                            startActivity(intent);
                            finish();
                        }


                    } else if (obj.optString("status").equals("false")) {
                        Toast.makeText(Act_Registration.this, obj.optString("message"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(Act_Registration.this, getString(R.string.something_wrong_please), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();
                Toast.makeText(Act_Registration.this, getString(R.string.something_wrong_please), Toast.LENGTH_LONG).show();
                Log.i("My error", "" + error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> map = new HashMap<String, String>();
                map.put("towtrack_name", st_nm);
//                map.put("email", st_email);
                map.put("password", st_pass);
                map.put("country_id", c_id);
                map.put("contact_no", st_mobile);
                Log.e("Params", map.toString());
                return map;
            }
        };
        Volley.newRequestQueue(getApplicationContext()).add(request);
        request.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    // firebase calback listener
    private void PhoneCallBack(boolean isOptSend) {

        callbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
//                Toast.makeText(getApplicationContext(), getString(R.string.send_otp), Toast.LENGTH_LONG).show();

                // instance firabase verify
                isInstant = true;
                authCredential = phoneAuthCredential;
                OtpDialog();

                String code = phoneAuthCredential.getSmsCode();
                if (sendOtpDialog != null){
                    sendOtpDialog.dismiss();
                }
            }

            @Override
            public void onVerificationFailed(@NonNull FirebaseException e) {
                if (isOptSend) {
                    Toast.makeText(getApplicationContext(), "Failed to send Opt", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Verification Failed", Toast.LENGTH_LONG).show();
                }

                if (e instanceof FirebaseTooManyRequestsException){
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                }

                if (sendOtpDialog != null){
                    sendOtpDialog.dismiss();
                }
            }

            @Override
            public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                super.onCodeSent(s, forceResendingToken);
                if (sendOtpDialog != null){
                    sendOtpDialog.dismiss();
                }
                // otp send
                Toast.makeText(getApplicationContext(), "Otp Send Successfully", Toast.LENGTH_LONG).show();
                token = forceResendingToken;
                verificationId = s;
                isVerified = false;
                OtpDialog();
            }
        };
    }

    // otp dialog show
    public void OtpDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(Act_Registration.this, R.style.AppCompatAlertDialogStyle);
        ViewGroup viewGroup = findViewById(android.R.id.content);

        View view = LayoutInflater.from(Act_Registration.this).inflate(R.layout.opt_verification_dialog, viewGroup, false);
        builder.setView(view);
        builder.setCancelable(false);

        dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);

        TextView txt_message = view.findViewById(R.id.txt_message);
        OtpView otpView = view.findViewById(R.id.otpView);
        TextView txt_timer = view.findViewById(R.id.txt_timer);
        AVLoadingIndicatorView av_loading = view.findViewById(R.id.av_loading);
        LinearLayout linBtn = view.findViewById(R.id.linBtn);
        otpView.setCursorVisible(false);
        otpView.setOtpCompletionListener(otp -> {
            AppUtils.hideKeyboard(Act_Registration.this);
        });

        // instance verify
        if (isInstant && authCredential != null){
            auth.signInWithCredential(authCredential)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                isInstant = false;

                                RegisterApi();
//                                        Toast.makeText(Act_Registration.this, getString(R.string.varification_successfully), Toast.LENGTH_SHORT).show();
                            } else {
                                if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                    Toast.makeText(Act_Registration.this, "Verification Failed, Invalid credentials", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(Act_Registration.this, "Verification Failed", Toast.LENGTH_SHORT).show();
                                }
                            }
                            linBtn.setVisibility(View.VISIBLE);
                            av_loading.setVisibility(View.GONE);
                        }
                    });
        }

        // otp verify menually
        startCountDown(txt_timer);
        otpView.setItemBackgroundColor(Color.BLACK);
        Button btnVerify = view.findViewById(R.id.btn_verify);
        Button btnCancel = view.findViewById(R.id.btn_cancel);
        Button btn_contact_us = view.findViewById(R.id.btn_contact_us);

        btn_contact_us.setOnClickListener(view1 -> {startActivity(new Intent(getApplicationContext(), Act_Contact_Us.class));});
        txt_message.setText(getString(R.string.please_type_the_varification_code_send_to)+ "\n " + tv_ph_code.getText().toString() + " " + ed_mobile.getText().toString());

        btnCancel.setOnClickListener(view1 -> {dialog.dismiss();});
        btnVerify.setOnClickListener(view1 -> {

            // register api error and btnVerify retry press
            if (isVerified) {
                RegisterApi();
            } else {
                linBtn.setVisibility(View.GONE);
                av_loading.setVisibility(View.VISIBLE);
                if (otpView.getText().toString().length() < 6) {
                    Toast.makeText(Act_Registration.this, getString(R.string.enter_otp), Toast.LENGTH_SHORT).show();
                    linBtn.setVisibility(View.VISIBLE);
                    av_loading.setVisibility(View.GONE);
                } else {

                    PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, otpView.getText().toString());
                    auth.signInWithCredential(credential)
                            .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        isVerified = true;
                                        RegisterApi();
                                        Toast.makeText(Act_Registration.this, getString(R.string.verification_successfuly), Toast.LENGTH_SHORT).show();
                                    } else {
                                        if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                            Toast.makeText(Act_Registration.this, "Verification Failed, Invalid credentials", Toast.LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(Act_Registration.this, "Verification Failed", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                    linBtn.setVisibility(View.VISIBLE);
                                    av_loading.setVisibility(View.GONE);
                                }
                            });
                }
            }
        });
        dialog.show();
    }

    // otp send timer
    public void startCountDown(TextView textView) {

        //60_000=60 sec or 1 min and another is interval of count down is 1 sec

        new CountDownTimer(120_000, 1000) {
            public void onTick(long millisUntilFinished) {
                long remainedSecs = millisUntilFinished / 1000;
                textView.setText(getString(R.string.enter_code_within) + (remainedSecs / 60) + ":" + (remainedSecs % 60) );
                textView.setTextColor(Color.BLACK);
                textView.setBackground(null);
            }
            public void onFinish() {
                textView.setText(getString(R.string.resend_otp));
                textView.setBackground(getResources().getDrawable(R.drawable.btm_line_shape));
                textView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PhoneAuthProvider.getInstance().verifyPhoneNumber(tv_ph_code.getText().toString() + st_mobile
                                , 2
                                , TimeUnit.MINUTES
                                , Act_Registration.this
                                , new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                                    @Override
                                    public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
                                        Toast.makeText(getApplicationContext(), getString(R.string.send_otp), Toast.LENGTH_LONG).show();
                                        String code = phoneAuthCredential.getSmsCode();
                                    }

                                    @Override
                                    public void onVerificationFailed(@NonNull FirebaseException e) {
                                        Log.e("tag Error", e.getMessage());
                                        e.printStackTrace();
                                        Toast.makeText(getApplicationContext(), "Failed to send Opt", Toast.LENGTH_LONG).show();
                                    }

                                    @Override
                                    public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                                        super.onCodeSent(s, forceResendingToken);
                                        token = forceResendingToken;
                                        verificationId = s;
                                        startCountDown(textView);
                                    }
                                },
                                token);
                    }
                });
            }
        }.start();
    }

    private boolean isValidNumber() {
        if (tv_ph_code.getText().toString().equals("+91") && ed_mobile.getText().toString().length() < 10) {
            ed_mobile.setError(getString(R.string.enter_valid_contact));
            return false;
        } else if (tv_ph_code.getText().equals("+962") && ed_mobile.getText().toString().length() < 9) {
            ed_mobile.setError(getString(R.string.enter_valid_contact));
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onClick(View view) {

        if (view == btn_reg) {
            Register();
        } else if (view == textView) {
            Intent intent = new Intent(Act_Registration.this, Act_Login.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
    }
}

