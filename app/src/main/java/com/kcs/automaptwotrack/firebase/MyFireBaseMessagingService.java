package com.kcs.automaptwotrack.firebase;

import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.kcs.automaptwotrack.Act_Admin_Message;
import com.kcs.automaptwotrack.Act_Tow_Track_Request;
import com.kcs.automaptwotrack.Navigation_Activity;
import com.kcs.automaptwotrack.R;

import org.json.JSONObject;

import java.util.Random;

import me.leolin.shortcutbadger.ShortcutBadger;

public class MyFireBaseMessagingService extends FirebaseMessagingService {

    private static final int REQUEST_CODE = 1;
    private NotificationUtils notificationUtils;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    int Notification_id = 0;
    int request_badge_count = 0, admin_msg_count;

    @SuppressLint("CommitPrefEdits")
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.e("receive_notification", "aaa");

        preferences = getSharedPreferences(getString(R.string.pref_Badge), MODE_PRIVATE);
        editor = preferences.edit();

        request_badge_count = preferences.getInt(getString(R.string.pref_badge_count), 0);
        admin_msg_count = preferences.getInt(getString(R.string.pref_admin_msg_badge_count), 0);

//        badge_count = badge_count + 1;

        /* Intent i = new Intent(this, Act_Edit_Profile_Detail.class);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, REQUEST_CODE,
                i, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "channel_id")
                .setContentText(remoteMessage.getNotification().getBody())
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setStyle(new NotificationCompat.BigTextStyle())
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());*/

        JSONObject obj = new JSONObject(remoteMessage.getData());
        String jsonMessage = obj.toString();
        Log.e("response", "JSON Object" + obj.toString());

        // count different request in badge with send notification
        try {
            String messageFor = obj.getString("message_for");
            Intent intent = new Intent("send_data");
            if (messageFor.equals("towtrack")) {
                Notification_id = 100;
                request_badge_count = request_badge_count + 1;
                editor.putInt(getString(R.string.pref_badge_count), request_badge_count);
                intent.putExtra(getString(R.string.pref_badge_count), request_badge_count);
                intent.putExtra(getString(R.string.pref_admin_msg_badge_count), admin_msg_count);
            } else if (messageFor.equals("admin_msg")) {
                Notification_id = new Random().nextInt();
                admin_msg_count = admin_msg_count + 1;
                editor.putInt(getString(R.string.pref_admin_msg_badge_count), admin_msg_count);
                intent.putExtra(getString(R.string.pref_badge_count), request_badge_count);
                intent.putExtra(getString(R.string.pref_admin_msg_badge_count), admin_msg_count);
            }
            editor.apply();
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
            ShortcutBadger.applyCount(getApplicationContext(), (admin_msg_count + request_badge_count));
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Calling method to generate notification
        sendNotification(jsonMessage);
    }

    // send notification
    private void sendNotification(String messageBody) {
        try {
            JSONObject jsonMain = new JSONObject(messageBody);
            String messageFor = "";
            String message = "";
            Intent intent = new Intent(this, Navigation_Activity.class);
            intent.putExtra("messageFor", messageFor);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            if (jsonMain.getString("success").equals("1")) {
                messageFor = jsonMain.getString("message_for");
                //messageFor = jsonMain.optString("message_for", "truck");
                message = jsonMain.optString("msg", "");

                if (messageFor.equals("admin_msg")){
                    intent = new Intent(this, Act_Admin_Message.class);
                    intent.putExtra("messageFor", messageFor);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                } else if (messageFor.equals("towtrack")){
                    intent = new Intent(this, Act_Tow_Track_Request.class);
                    intent.putExtra("messageFor", messageFor);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                }
            }

            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
            showNotificationMessage(getApplicationContext(), getString(R.string.app_name), message, "" + System.currentTimeMillis(), intent,(request_badge_count+admin_msg_count));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // redirect notification
    private void showNotificationMessage(Context context, String title, String message, String timeStamp, @NonNull Intent intent, int badge_count) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, Notification_id, message, timeStamp, intent, badge_count);
    }
}


